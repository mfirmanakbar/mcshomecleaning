package mcs.mitratama.com.mcshomecleaning.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mcs.mitratama.com.mcshomecleaning.R;
import mcs.mitratama.com.mcshomecleaning.data.dataHistory2;

/**
 * Created by koko on 7/13/16.
 */
public class HistoryAdapter2 extends BaseAdapter {

    Context c;
    ArrayList<dataHistory2> items;
    LayoutInflater inflater;
    TextView txtHistoryStatus;

    public HistoryAdapter2(Context c, ArrayList<dataHistory2> item) {
        this.c = c;
        this.items = item;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null)
            inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null)
            convertView=inflater.inflate(R.layout.item_history,parent,false);

        TextView txtHistoryNoInvoice = (TextView)convertView.findViewById(R.id.txtHistoryNoInvoice);
        TextView txtHistoryDate = (TextView)convertView.findViewById(R.id.txtHistoryDate);
        TextView txtHistoryTime = (TextView)convertView.findViewById(R.id.txtHistoryTime);
        txtHistoryStatus = (TextView)convertView.findViewById(R.id.txtHistoryStatus);

        dataHistory2 data = items.get(position);
        txtHistoryNoInvoice.setText(data.getId_invoice2());
        txtHistoryDate.setText(data.getDated2());
        txtHistoryTime.setText(data.getTimed2());
        String status = data.getFlag2();

        if(data.getFlag2().equals("1")){
            txtHistoryStatus.setText("Ordered");
            txtHistoryStatus.setBackgroundResource(R.color.colorBiru);
        }else if(data.getFlag2().equals("2")){
            txtHistoryStatus.setText("Process");
            txtHistoryStatus.setBackgroundResource(R.color.colorKuning);
        }else if(data.getFlag2().equals("3")){
            txtHistoryStatus.setText("Success");
            txtHistoryStatus.setBackgroundResource(R.color.colorHijau);
        }else if(data.getFlag2().equals("4")){
            txtHistoryStatus.setText("Canceled");
            txtHistoryStatus.setBackgroundResource(R.color.colorRed);
        }

        return convertView;
    }
}
