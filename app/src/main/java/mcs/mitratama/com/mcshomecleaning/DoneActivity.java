package mcs.mitratama.com.mcshomecleaning;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import mcs.mitratama.com.mcshomecleaning.sqlite.DbRoom;
import mcs.mitratama.com.mcshomecleaning.util.Server;

public class DoneActivity extends AppCompatActivity {

    TextView txtDoneInvoice, atminfo;
    private JSONObject jsonObject2;
    private ProgressDialog dialog2 = null;
    String id_invoice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);

        txtDoneInvoice = (TextView)findViewById(R.id.txtDoneInvoice);
        atminfo = (TextView)findViewById(R.id.atminfo);

        jsonObject2 = new JSONObject();

        dialog2 = new ProgressDialog(this);
        dialog2.setMessage("Loading Order Room...");
        dialog2.setCancelable(false);

        if(RoomActivity.donePaymentTypeTrue.equals("Cash")){
            atminfo.setVisibility(View.GONE);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id_invoice = extras.getString("id_invoice");
            txtDoneInvoice.setText(Html.fromHtml("Terimakasih atas kepercayaannya telah memilih MCS Home Cleaning. Pesanan Anda sudah kami terima. Order ID Anda adalah <b>#"+id_invoice+"</b>"));
        }

        saveRorder();

        Button btnDoneToHome = (Button)findViewById(R.id.btnDoneToHome);
        btnDoneToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoomActivity.RoomActivity.finish();
                KategoriRoomActivity.KategoriRoomActivity.finish();
                OrderActivity.OrderActivity.finish();
                finish();
            }
        });

        LinearLayout tekanCall = (LinearLayout)findViewById(R.id.tekanCall);
        tekanCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Intent.ACTION_CALL, Uri.parse("tel:0217668413"));
                try{
                    startActivity(in);
                }catch (android.content.ActivityNotFoundException ex){
                    Toast.makeText(getApplicationContext(),"Your activity is not founded!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        RoomActivity.RoomActivity.finish();
        KategoriRoomActivity.KategoriRoomActivity.finish();
        OrderActivity.OrderActivity.finish();
    }

    private void saveRorder() {

        DbRoom db = new DbRoom(DoneActivity.this);
        db.open();
        Cursor cur = db.getAllRoom();
        if(cur.getCount()!=0){
            DbRoom db2 = new DbRoom(DoneActivity.this);
            db2.open();
            Cursor cur2 = db.getAllRoom();
            if (cur2 != null) {
                if (cur2.moveToFirst()) {
                    if (cur2.getCount() != 0) {

                        do {

                            Log.d("saveRorder_sql", cur2.getString(0) + ", " + cur2.getString(1) + ", " + cur2.getString(2) + ", " + cur2.getString(3) + ", " + cur2.getString(4) + ", " + cur2.getString(5));
                            saveNya(cur2.getString(1),cur2.getString(2),cur2.getString(3),cur2.getString(4),cur2.getString(5));

                        } while (cur2.moveToNext());
                    } else {}
                }
            } else {}
            db.close();
        }else {}
        db.close();

    }

    public void saveNya(final String s1, final String s2, final String s3, final String s4, final String s5) {

        try {
            final String xDoneInvoice = id_invoice;//txtDoneInvoice.getText().toString();
            Log.e("donedone",xDoneInvoice);
            jsonObject2.put("id_invoice", xDoneInvoice);
            jsonObject2.put("id_room", s1);
            jsonObject2.put("nama_room", s2);
            jsonObject2.put("estimasi_waktu", s3);
            jsonObject2.put("jumlah_room", s4);
            jsonObject2.put("harga_bayar", s5);
        } catch (JSONException e) {
            Log.e("saveRorder_error", e.toString());
        }
        dialog2.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.order_r, jsonObject2,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("saveRorder_response", jsonObject.toString());
                        try {
                            Boolean success = jsonObject.getBoolean("success");
                            String info = jsonObject.getString("info");
                            Log.e("orderRNow_result", success+", "+info);
                            if(success==true){
                                //Toast.makeText(getApplication(), "Order room successful!", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(getApplication(), "Order room failed!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog2.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog2.dismiss();
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
            }
        });jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }


}
