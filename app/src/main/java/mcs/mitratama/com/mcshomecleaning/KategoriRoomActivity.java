package mcs.mitratama.com.mcshomecleaning;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mcs.mitratama.com.mcshomecleaning.adapter.AdapterKategoriRoom;
import mcs.mitratama.com.mcshomecleaning.adapter.AdapterRoom;
import mcs.mitratama.com.mcshomecleaning.adapter.AdapterSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataKategoriRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.util.Server;

public class KategoriRoomActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    ListView list;
    AdapterKategoriRoom adapter;
    List<dataKategoriRoom> itemList = new ArrayList<dataKategoriRoom>();
    SwipeRefreshLayout swipe;
    public static Activity KategoriRoomActivity;
    public static String xId_kategori_room;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategori_room);

        KategoriRoomActivity = KategoriRoomActivity.this;

        swipe   = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_listViewKategoriRoomnyaa);
        list    = (ListView) findViewById(R.id.listViewKategoriRoomnyaa);
        adapter = new AdapterKategoriRoom(KategoriRoomActivity.this, (ArrayList<dataKategoriRoom>) itemList);
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(this);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                xId_kategori_room = "0";
                final String xId = itemList.get(i).getKategori_room_id();
                final String xNama = itemList.get(i).getKategori_room_nama();
                //Toast.makeText(KategoriRoomActivity.this,xId +". "+xNama,Toast.LENGTH_SHORT).show();
                xId_kategori_room = xId;
                Intent a = new Intent(KategoriRoomActivity.this, ListRoomActivity.class);
                startActivity(a);
                finish();
            }
        });
    }

    private void kategoriRoom() {

        swipe.setRefreshing(true);
        itemList.clear();
        adapter.notifyDataSetChanged();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Server.room_kategori, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            Log.e("kategoriRoom", response.toString());
                            JSONArray jsonArrayContent = response.getJSONArray("content");
                            for(int i=0; i<jsonArrayContent.length();i++){
                                try {
                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String Kategori_room_id = jsonObjectContent.getString("id");
                                    String Kategori_room_nama = jsonObjectContent.getString("nama");

                                    Log.e("kategoriRoom", Kategori_room_id+". "+Kategori_room_nama);

                                    dataKategoriRoom items = new dataKategoriRoom();
                                    items.setKategori_room_id(Kategori_room_id);
                                    items.setKategori_room_nama(Kategori_room_nama);
                                    itemList.add(items);

                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        }
                        catch (JSONException e){
                            Log.e("error", e.getMessage());
                        }
                        adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage(), getApplication());
                swipe.setRefreshing(false);
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    @Override
    public void onRefresh() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        kategoriRoom();
    }

    @Override
    protected void onResume() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        kategoriRoom();
        super.onResume();
    }
}
