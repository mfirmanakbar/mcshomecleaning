package mcs.mitratama.com.mcshomecleaning;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mcs.mitratama.com.mcshomecleaning.adapter.AdapterSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.util.Server;

public class HistoryDetailActivity extends AppCompatActivity {

    TextView txtHistoryDetaiNoInvoice, txtHistoryDetailMyLocation, txtHistoryDetailAddressDetail,
            txtHistoryDetailDate, txtHistoryDetailTime, txtHistoryDetailNote,
            txtHistoryDetailNamaPekerja,txtHistoryDetailTotalBayar,
            txtHistoryDetailAtmInfo, txtHistoryDetailGenderCleaner;
    Button btnHistoryDetailFinish;

    ListView list;
    AdapterSqliteRoom adapter;
    List<dataSqliteRoom> itemList = new ArrayList<dataSqliteRoom>();
    Context mContext=this;
    private JSONObject jsonObject, jsonObject2;
    //private ProgressDialog dialog = null;
    String xflag, xid_invoice;
    int xreview;

    AlertDialog.Builder abdialog;
    LayoutInflater abinflater;
    View abdialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        txtHistoryDetaiNoInvoice = (TextView)findViewById(R.id.txtHistoryDetaiNoInvoice);
        txtHistoryDetailMyLocation = (TextView)findViewById(R.id.txtHistoryDetailMyLocation);
        txtHistoryDetailAddressDetail = (TextView)findViewById(R.id.txtHistoryDetailAddressDetail);
        txtHistoryDetailDate = (TextView)findViewById(R.id.txtHistoryDetailDate);
        txtHistoryDetailTime = (TextView)findViewById(R.id.txtHistoryDetailTime);
        txtHistoryDetailNote = (TextView)findViewById(R.id.txtHistoryDetailNote);
        txtHistoryDetailNamaPekerja = (TextView)findViewById(R.id.txtHistoryDetailNamaPekerja);
        txtHistoryDetailTotalBayar = (TextView)findViewById(R.id.txtHistoryDetailTotalBayar);
        txtHistoryDetailAtmInfo = (TextView)findViewById(R.id.txtHistoryDetailAtmInfo);
        btnHistoryDetailFinish = (Button)findViewById(R.id.btnHistoryDetailFinish);
        txtHistoryDetailGenderCleaner = (TextView)findViewById(R.id.txtHistoryDetailGenderCleaner);

        list    = (ListView) findViewById(R.id.listViewHistoryDetail);
        adapter = new AdapterSqliteRoom(HistoryDetailActivity.this, (ArrayList<dataSqliteRoom>) itemList);
        list.setAdapter(adapter);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            xid_invoice = extras.getString("xid_invoice");
            String xalamat = extras.getString("xalamat");
            String xdetail_alamat = extras.getString("xdetail_alamat");
            String xdated = extras.getString("xdated");
            String xtimed = extras.getString("xtimed");
            String xnote = extras.getString("xnote");
            String xtotal_jumlah_room = extras.getString("xtotal_jumlah_room");
            String xtotal_estimasi_waktu = extras.getString("xtotal_estimasi_waktu");
            String xtype_payment = extras.getString("xtype_payment");
            String xtotal_pembayaran = extras.getString("xtotal_pembayaran");
            String xnama_pekerja = extras.getString("xnama_pekerja");
            String xgender_cleaner = extras.getString("xgender_cleaner");
            xflag = extras.getString("xflag");
            xreview = extras.getInt("xreview");

            Log.e("xtype_payment",xtype_payment);

            if(xtype_payment.equals("Transfer")){
                txtHistoryDetailAtmInfo.setVisibility(View.VISIBLE);
            }

            txtHistoryDetaiNoInvoice.setText(xid_invoice);
            txtHistoryDetailMyLocation.setText(xalamat);
            txtHistoryDetailAddressDetail.setText(xdetail_alamat);
            txtHistoryDetailDate.setText(xdated);
            txtHistoryDetailTime.setText(xtimed);
            txtHistoryDetailNote.setText(xnote);
            txtHistoryDetailNamaPekerja.setText(xnama_pekerja);
            txtHistoryDetailGenderCleaner.setText(xgender_cleaner);
            txtHistoryDetailTotalBayar.setText(xtotal_jumlah_room+" rooms, "+xtotal_estimasi_waktu+" minutes, "+ xtype_payment+ " Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(String.valueOf(xtotal_pembayaran))));

            if(xnama_pekerja.equals("null") || xnama_pekerja.equals(null)){
                txtHistoryDetailNamaPekerja.setText("Menunggu data karyawan ...");
                txtHistoryDetailNamaPekerja.setTextColor(getResources().getColor(R.color.colorRed));
            }

            if(xflag.equals("1") || xflag.equals("2")){
                btnHistoryDetailFinish.setText("CANCEL");
            }else if(xflag.equals("3")) {
                if(xreview>0){
                    btnHistoryDetailFinish.setVisibility(View.GONE);
                }else {
                    btnHistoryDetailFinish.setText("GIVE RATING");
                }
            }else if(xflag.equals("4")){
                btnHistoryDetailFinish.setVisibility(View.GONE);
            }

            btnHistoryDetailFinish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(xflag.equals("1") || xflag.equals("2")) {
                        cancelOrder(xid_invoice);
                    }else if(xflag.equals("3")) {
                        giveRating(xid_invoice);
                    }
                }
            });
            loadROder(xid_invoice);
        }
    }

    private void giveRating(final String xid_invoice) {
        abdialog = new AlertDialog.Builder(HistoryDetailActivity.this);
        abinflater = this.getLayoutInflater();
        abdialogView = abinflater.inflate(R.layout.rating, null);
        abdialog.setView(abdialogView);
        abdialog.setCancelable(true);
        abdialog.setIcon(R.mipmap.ic_launcher);
        abdialog.setTitle("Thanks for Feedback");

        final RatingBar ratingBarOrder = (RatingBar) abdialogView.findViewById(R.id.ratingBarOrder);
        final EditText txtRating = (EditText) abdialogView.findViewById(R.id.txtRating);
        final Button btnRating = (Button) abdialogView.findViewById(R.id.btnRating);

        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = txtRating.getText().toString();
                String rating = String.valueOf(ratingBarOrder.getRating());
                Toast.makeText(HistoryDetailActivity.this,rating+" "+xid_invoice+" "+note,Toast.LENGTH_LONG).show();
                finish();
            }
        });
        abdialog.show();
    }

    private void cancelOrder(final String xid_invoice) {
        jsonObject2 = new JSONObject();
        try {
            jsonObject2.put("id_invoice", xid_invoice);
        } catch (JSONException e) {
            Log.e("cancelOrder", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.order_cancel, jsonObject2,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        //Log.e("cancelOrder", jsonObject.toString());
                        try {
                            Boolean success = jsonObject.getBoolean("success");
                            String info = jsonObject.getString("info");

                            Log.e("cancelOrder", String.valueOf(success));
                            Log.e("cancelOrder", info);

                            if(success==false){
                                Toast.makeText(getApplication(), "Failed! "+info, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(getApplication(), "Successfuly! "+info, Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplication(), "Error to connect! [Cancel Order]", Toast.LENGTH_SHORT).show();
                Log.e("cancelOrder", volleyError.toString());
                finish();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    private void loadROder(final String xid_invoice) {

        itemList.clear();
        adapter.notifyDataSetChanged();
        jsonObject = new JSONObject();
        //dialog.show();
        try {
            jsonObject.put("id_invoice", xid_invoice);
        } catch (JSONException e) {
            Log.e("loadROder", e.toString());
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.list_order_r, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("loadROder", jsonObject.toString());
                        try {
                            Boolean success = jsonObject.getBoolean("success");
                            String info = jsonObject.getString("info");
                            JSONArray jsonArrayContent = jsonObject.getJSONArray("content");

                            //Log.e("loadROder", String.valueOf(success));
                            //Log.e("loadROder", info);

                            if(success==true){
                                //Toast.makeText(getApplication(), "Order Successful!", Toast.LENGTH_SHORT).show();
                                Log.e("loadROder", "y1");

                                for(int i=0; i<jsonArrayContent.length();i++){
                                    try {

                                        JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                        String id_room = jsonObjectContent.getString("id_room");
                                        String nama_room = jsonObjectContent.getString("nama_room");
                                        int estimasi_waktu = jsonObjectContent.getInt("estimasi_waktu");
                                        int jumlah_room = jsonObjectContent.getInt("jumlah_room");
                                        int harga_bayar = jsonObjectContent.getInt("harga_bayar");

                                        //Log.e("loadROder", "y2");
                                        //Log.e("loadROder", nama_room);

                                        dataSqliteRoom item = new dataSqliteRoom();
                                        item.setId("");
                                        item.setId_room(id_room);
                                        item.setNama(nama_room);
                                        item.setWaktuestimasi(estimasi_waktu);
                                        item.setTotalroom(jumlah_room);
                                        item.setTotalbayar(harga_bayar);
                                        itemList.add(item);

                                    }catch (JSONException e){
                                        e.printStackTrace();
                                    }
                                }

                            }else {
                                Toast.makeText(getApplication(), "Failed!", Toast.LENGTH_SHORT).show();
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //dialog.dismiss();
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
            }
        })

                ;jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);

    }
}
