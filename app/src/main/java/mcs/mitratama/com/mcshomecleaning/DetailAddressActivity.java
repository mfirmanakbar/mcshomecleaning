package mcs.mitratama.com.mcshomecleaning;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class DetailAddressActivity extends AppCompatActivity {

    EditText txtAddressDetailSet;
    public static String AD="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_address);

        txtAddressDetailSet = (EditText)findViewById(R.id.txtAddressDetailSet);
        txtAddressDetailSet.setText(AD.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_done) {
            AD = txtAddressDetailSet.getText().toString();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
