package mcs.mitratama.com.mcshomecleaning.data;

/**
 * Created by koko on 7/10/16.
 */
public class dataRoom {
    private String id, id_area, nama, des, time, harga, area;

    public dataRoom() {
    }

    public dataRoom(String id, String id_area, String nama, String des, String time, String harga, String area) {
        this.id = id;
        this.id_area = id_area;
        this.nama = nama;
        this.des = des;
        this.time = time;
        this.harga = harga;
        this.area = area;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_area() {
        return id_area;
    }

    public void setId_area(String id_area) {
        this.id_area = id_area;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
