package mcs.mitratama.com.mcshomecleaning;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import mcs.mitratama.com.mcshomecleaning.sqlite.DbRoom;
import pl.droidsonroids.gif.GifTextView;

public class HomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        TimePickerDialog.OnTimeSetListener {

    LinearLayout setWaktu, setTanggal, setDetAddress;
    Button btnHomeToRoom;
    TextView txtAddressDetailGett, txtLokasiSaya;
    int mYear, mMonth, mDay, mHour, mMinutes, mSecond;
    SharedPreferences sp;
    TextView txtHomeEmail, txtHomeNama;
    TextView txtHomeDate, txtHomeTime;

    public static String doneAddresstrue, doneDetailAddresstrue, doneDatetrue, doneDatefalse, doneTimetrue, doneIdUsertrue;
    public static double doneLattrue, doneLongtrue;

    public static Activity HomeActivity;

    GoogleMap mMap;
    Context mContext;
    private LatLng mCenterLatLong;
    private static String TAG = "MAP_LOCATION";
    Location mLocation;
    GifTextView loadingMapsGif;
    GoogleApiClient mGoogleApiClient;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    TextView mLocationMarkerText;
    private AddressResultReceiver mResultReceiver;
    protected String mAddressOutput;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    //EditText mLocationAddress;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;

    /*AlertDialog.Builder abdialog;
    LayoutInflater abinflater;
    View abdialogView;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        HomeActivity = HomeActivity.this;

        doneIdUsertrue = "";
        doneAddresstrue = "";
        doneDetailAddresstrue = "";
        doneDatetrue = "";
        doneDatefalse = "";
        doneTimetrue = "";
        DetailAddressActivity.AD = "";
        doneLattrue = 0;
        doneLongtrue = 0;

        sp = getSharedPreferences("sesiLoginMcs", Context.MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadingMapsGif = (GifTextView) findViewById(R.id.loadingMapsGif);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        txtHomeEmail = (TextView) header.findViewById(R.id.txtHomeEmail);
        txtHomeNama = (TextView) header.findViewById(R.id.txtHomeNama);

        doneIdUsertrue = sp.getString("sesiLoginMcs_id", null);
        Log.d("doneIdUsertrue", doneIdUsertrue);
        String sesiLoginMcs_email = sp.getString("sesiLoginMcs_email", null);
        String sesiLoginMcs_nama = sp.getString("sesiLoginMcs_nama", null);
        txtHomeEmail.setText(sesiLoginMcs_email);
        txtHomeNama.setText(sesiLoginMcs_nama);

        setWaktu = (LinearLayout) findViewById(R.id.setWaktu);
        setTanggal = (LinearLayout) findViewById(R.id.setTanggal);
        btnHomeToRoom = (Button) findViewById(R.id.btnHomeToRoom);
        setDetAddress = (LinearLayout) findViewById(R.id.setDetAddress);
        txtAddressDetailGett = (TextView) findViewById(R.id.txtAddressDetailGet);
        txtLokasiSaya = (TextView) findViewById(R.id.txtLokasiSaya);
        txtHomeDate = (TextView) findViewById(R.id.txtHomeDate);
        txtHomeTime = (TextView) findViewById(R.id.txtHomeTime);

        btnHomeToRoom.setVisibility(View.GONE);

        setWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });
        setTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        btnHomeToRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasi();
            }
        });
        setDetAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent a = new Intent(getApplication(), DetailAddressActivity.class);
                startActivity(a);*/
                callPopupAddressDet();
            }
        });

        /*BEGIN MAPS*/

        mContext = this;
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapCenter);
        mapFragment.getMapAsync(this);
        mResultReceiver = new AddressResultReceiver(new Handler());
        if (checkPlayServices()) {
            if (!AppUtils.isLocationEnabled(mContext)) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
                dialog.setMessage("Location not enabled!");
                dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                    }
                });
                dialog.show();
            }
            buildGoogleApiClient();
        } else {
            Toast.makeText(mContext, "Location not supported in this device", Toast.LENGTH_SHORT).show();
        }

        /*ENDING MAPS*/


        /*Calendar cekCal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(cekCal.getTime());
        String formattedTime = dt.format(cekCal.getTime());
        Toast.makeText(this, formattedDate + " AND " + formattedTime, Toast.LENGTH_SHORT).show();
        Log.e("waktu", formattedDate + " AND " + formattedTime);*/

    }

    private void callPopupAddressDet() {
        //AlertDialog.Builder abdialog;
        //LayoutInflater abinflater;
        //View abdialogView;
        final AlertDialog.Builder abdialog = new AlertDialog.Builder(HomeActivity.this);
        LayoutInflater abinflater = this.getLayoutInflater();
        View abdialogView = abinflater.inflate(R.layout.addressdetpopup, null);
        abdialog.setView(abdialogView);
        abdialog.setCancelable(true);
        abdialog.setIcon(R.mipmap.ic_launcher);
        abdialog.setTitle("Detail Address");
        final EditText txtDetAddPopup = (EditText) abdialogView.findViewById(R.id.txtDetAddPopup);
        abdialog.setNeutralButton("SUBMIT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                txtAddressDetailGett.setText(txtDetAddPopup.getText().toString());
                dialog.cancel();
            }
        });
        abdialog.show();
    }

    private void validasi() {
        doneDetailAddresstrue = txtAddressDetailGett.getText().toString();
        if (doneAddresstrue.isEmpty()) {
            Toast.makeText(getApplication(), "Internet error! ", Toast.LENGTH_SHORT).show();
        } else if (doneDetailAddresstrue.isEmpty()) {
            Toast.makeText(getApplication(), "Please input your detail adress! ", Toast.LENGTH_SHORT).show();
        } else if (doneDatetrue.isEmpty()) {
            Toast.makeText(getApplication(), "Please choose date of order! ", Toast.LENGTH_SHORT).show();
        } else if (doneDatefalse.isEmpty()) {
            Toast.makeText(getApplication(), "Please choose date of order! ", Toast.LENGTH_SHORT).show();
        } else if (doneTimetrue.isEmpty()) {
            Toast.makeText(getApplication(), "Please choose time of order! ", Toast.LENGTH_SHORT).show();
        } else {
            DbRoom db = new DbRoom(HomeActivity.this);
            db.open();
            db.deleteAllRoom();
            db.close();
            Intent a = new Intent(getApplication(), RoomActivity.class);
            startActivity(a);
        }
    }

    @Override
    protected void onResume() {
        txtAddressDetailGett.setText(DetailAddressActivity.AD.toString());
        //doneDetailAddresstrue = DetailAddressActivity.AD.toString();
        doneDetailAddresstrue = txtAddressDetailGett.getText().toString();
        super.onResume();
    }

    public void showTimePickerDialog() {

        final Calendar finalcekCal = Calendar.getInstance();
        SimpleDateFormat finaljamx = new SimpleDateFormat("HH");
        SimpleDateFormat finalmenitx = new SimpleDateFormat("mm");
        SimpleDateFormat finaldetikx = new SimpleDateFormat("ss");
        mHour = Integer.parseInt(finaljamx.format(finalcekCal.getTime()));
        mMinutes = Integer.parseInt(finalmenitx.format(finalcekCal.getTime()));
        mSecond = Integer.parseInt(finaldetikx.format(finalcekCal.getTime()));

        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay < 9 || hourOfDay > 17) {
                            txtHomeTime.setText("Time...");
                            Toast.makeText(getApplication(), "Jam operasi hanya 9.00 AM - 5.00 PM", Toast.LENGTH_LONG).show();
                        }else {
                            String minuteString = null;
                            if (minute == 00) {
                                minuteString = "00";
                            } else if (minute < 10 && minute > 0) {
                                minuteString = "0" + String.valueOf(minute);
                            } else {
                                minuteString = String.valueOf(minute);
                            }
                            txtHomeTime.setText(String.valueOf(hourOfDay) + ":" + minuteString);
                            doneTimetrue = String.valueOf(hourOfDay) + ":" + minuteString;
                        }
                    }
                }, mHour, mMinutes, false);
        tpd.show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int bulan = monthOfYear + 1;
                        String bulanString = "";
                        if (bulan == 1) {
                            bulanString = "January";
                        } else if (bulan == 2) {
                            bulanString = "February";
                        } else if (bulan == 3) {
                            bulanString = "March";
                        } else if (bulan == 4) {
                            bulanString = "April";
                        } else if (bulan == 5) {
                            bulanString = "May";
                        } else if (bulan == 6) {
                            bulanString = "June";
                        } else if (bulan == 7) {
                            bulanString = "July";
                        } else if (bulan == 8) {
                            bulanString = "August";
                        } else if (bulan == 9) {
                            bulanString = "September";
                        } else if (bulan == 10) {
                            bulanString = "October";
                        } else if (bulan == 11) {
                            bulanString = "November";
                        } else if (bulan == 12) {
                            bulanString = "December";
                        }

                        Calendar cekCal = Calendar.getInstance();
                        SimpleDateFormat tanggalx = new SimpleDateFormat("dd");
                        SimpleDateFormat bulanxz = new SimpleDateFormat("MM");
                        int xtanggal = Integer.parseInt(tanggalx.format(cekCal.getTime()));
                        int xbulan = Integer.parseInt(bulanxz.format(cekCal.getTime()));

                        if (dayOfMonth < xtanggal) {
                            Toast.makeText(HomeActivity.this, "Wrong past date!", Toast.LENGTH_SHORT).show();
                        }else if (dayOfMonth < xtanggal && bulan > xbulan) {
                            txtHomeDate.setText(dayOfMonth + " " + bulanString + " " + year);
                            doneDatetrue = dayOfMonth + " " + bulanString + " " + year;
                            doneDatefalse = year + "-" + bulan + "-" + dayOfMonth;
                        }else {
                            txtHomeDate.setText(dayOfMonth + " " + bulanString + " " + year);
                            doneDatetrue = dayOfMonth + " " + bulanString + " " + year;
                            doneDatefalse = year + "-" + bulan + "-" + dayOfMonth;
                        }
                    }
                }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpd.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            Intent a = new Intent(HomeActivity.this, HomeActivity.class);
            startActivity(a);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_profile) {
            Intent a = new Intent(HomeActivity.this, ProfileActivity.class);
            startActivity(a);
        } else if (id == R.id.nav_history) {
            Intent a = new Intent(HomeActivity.this, HistoriesActivity.class);
            startActivity(a);
        } else if (id == R.id.nav_term) {
            Intent a = new Intent(HomeActivity.this, TermActivity.class);
            startActivity(a);
        } else if (id == R.id.nav_call) {
            Intent a = new Intent(HomeActivity.this, DetailContactActivity.class);
            startActivity(a);
            /*Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:0217668413"));
            try {
                startActivity(in);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Your activity is not founded!", Toast.LENGTH_SHORT).show();
            }*/
        } else if (id == R.id.nav_logout) {
            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            editor.commit();
            Intent a = new Intent(HomeActivity.this, LoginActivity.class);
            startActivity(a);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.d("Camera postion change" + "", cameraPosition + "");
                mCenterLatLong = cameraPosition.target;
                mMap.clear();
                try {
                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);
                    startIntentService(mLocation);
                    getCompleteAddressString(mLocation.getLatitude(),mLocation.getLongitude());
                    //mLocationMarkerText.setText("Lat : " + mCenterLatLong.latitude + "," + "Long : " + mCenterLatLong.longitude);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            changeMap(mLastLocation);
            Log.d(TAG, "ON connected");

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (location != null)
                changeMap(location);
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {

        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //finish();
            }
            return false;
        }
        return true;
    }

    private void changeMap(Location location) {
        Log.d(TAG, "Reaching map" + mMap);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(false);
            LatLng latLong;


            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLong).zoom(17f).build();
            //CameraPosition cameraPosition = new CameraPosition.Builder().target(latLong).zoom(19f).tilt(70).build();

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            //mLocationMarkerText.setText("Lat : " + location.getLatitude() + "," + "Long : " + location.getLongitude());
            getCompleteAddressString(location.getLatitude(),location.getLongitude());
            startIntentService(location);


        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);
            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);
            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);
            displayAddressOutput();
            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
            }
        }
    }

    protected void displayAddressOutput() {
        try {
            if (mAreaOutput != null)
                Log.e("oops",mAddressOutput);// E F
                //txtLokasiSaya.setText(mAddressOutput);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void startIntentService(Location mLocation) {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);
        startService(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(mContext, data);
                LatLng latLong;
                latLong = place.getLatLng();

                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLong).zoom(17f).build();
                //CameraPosition cameraPosition = new CameraPosition.Builder().target(latLong).zoom(19f).tilt(70).build();

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(mContext, data);
        } else if (resultCode == RESULT_CANCELED) {
            // Indicates that the activity closed before a selection was made. For example if
            // the user pressed the back button.
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        String postalCode = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString();
                //postalCode = addresses.get(0).getPostalCode();
                String dkij = addresses.get(0).getAddressLine(4);
                //String hasil_addressing = "" + strReturnedAddress.toString() + postalCode;
                String hasil_addressing = "" + strReturnedAddress.toString();

                String numbers = dkij.substring(0, Math.min(dkij.length(), 29));

                Log.d("dkij", dkij + " _NOW_ "+numbers);

                if(numbers.equals("Daerah Khusus Ibukota Jakarta")){
                    btnHomeToRoom.setVisibility(View.VISIBLE);
                }else{
                    btnHomeToRoom.setVisibility(View.GONE);
                    Toast.makeText(getApplication(), "Maaf, kami baru dapat menerima order area DKI Jakarta.", Toast.LENGTH_SHORT).show();
                }

                txtLokasiSaya.setText(hasil_addressing.toString());

                if(loadingMapsGif.getVisibility()==View.VISIBLE){
                    loadingMapsGif.setVisibility(View.GONE);
                }

                doneAddresstrue = hasil_addressing.toString();
                doneLattrue = LATITUDE;
                doneLongtrue = LONGITUDE;

                Log.d("alamat", hasil_addressing);
            } else {
                Log.d("alamat", "Tidak ada alamat yang terdaftar!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("alamat", "Canont get Address!");
        }
        return strAdd;
    }

}
