package mcs.mitratama.com.mcshomecleaning;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import mcs.mitratama.com.mcshomecleaning.util.Server;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    TextView txtSignUp, txtForget;
    EditText txtLoginEmail, txtLoginPassword;
    private JSONObject jsonObject;
    private ProgressDialog dialog = null;
    Boolean success;
    String info;
    SharedPreferences sp;

    AlertDialog.Builder dialogg;
    LayoutInflater inflaterr;
    View dialogVieww;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = (Button)findViewById(R.id.btnLoginLogin);
        txtSignUp = (TextView)findViewById(R.id.txtViewSignUp);
        txtForget = (TextView)findViewById(R.id.txtViewForget);
        txtLoginEmail = (EditText)findViewById(R.id.txtLoginEmail);
        txtLoginPassword = (EditText)findViewById(R.id.txtLoginPassword);

        sp = getSharedPreferences("sesiLoginMcs", Context.MODE_PRIVATE);
        cekSesiLogin();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginNow();
            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(a);
            }
        });

        txtForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                startActivity(a);
            }
        });

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading Login...");
        dialog.setCancelable(false);
        jsonObject = new JSONObject();

    }

    private boolean isValidEmaillId(String email){
        return Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches();
    }

    private void cekSesiLogin() {
        String sesiLoginMcs_id = sp.getString("sesiLoginMcs_id", null);
        if(sesiLoginMcs_id!=null){
            Intent a = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(a);
            finish();
        }
    }

    private void LoginNow() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(LoginActivity.this, "Mohon hidupkan GPS Anda.", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }else{
            validasi();
        }
    }

    private void validasi() {
        String loginEmail = txtLoginEmail.getText().toString();
        String loginPassword = txtLoginPassword.getText().toString();
        if(loginEmail.length() == 0){
            txtLoginEmail.setError("Please input your email!");Toast.makeText(getApplication(),"Please input your email!",Toast.LENGTH_LONG).show();
        }else if(loginPassword.length() == 0){
            txtLoginPassword.setError("Please input your password!");Toast.makeText(getApplication(),"Please input your password!",Toast.LENGTH_LONG).show();
        }else if(isValidEmaillId(txtLoginEmail.getText().toString().trim())==false){
            txtLoginEmail.setError("Invalid email address!");Toast.makeText(getApplication(),"Invalid email address!",Toast.LENGTH_LONG).show();
        }else{
            cekDatabaseLogin(loginEmail,loginPassword);
        }
    }

    private void cekDatabaseLogin(final String loginEmail, final String loginPassword) {
        dialog.show();
        try {
            jsonObject.put("email", loginEmail.trim());
            jsonObject.put("password", loginPassword.trim());
            Log.d("cekDatabaseLogin", loginEmail.trim() + ", " + loginPassword.trim());
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        }JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.login_user, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("cekDatabaseLogin1", jsonObject.toString());
                        try {
                            success = jsonObject.getBoolean("success");
                            info = jsonObject.getString("info");
                            Log.e("cekDatabaseLogin2", success+", "+info);
                            if(success==true){
                                Log.e("cekDatabaseLogin3", "Masuk");
                                Toast.makeText(LoginActivity.this, "Successful!", Toast.LENGTH_SHORT).show();
                                JSONArray content = jsonObject.getJSONArray("content");
                                    JSONObject content_jsonObject = content.getJSONObject(0);
                                    String id = content_jsonObject.getString("id");
                                    String nama = content_jsonObject.getString("nama");
                                    String email = content_jsonObject.getString("email");
                                    Log.e("cekDatabaseLogin3", success + ", " + info + ", " + id + ", " + nama + ", " + email);
                                saveToSession(id, nama, email);
                            }else {
                                Toast.makeText(LoginActivity.this, "Wrong email or password!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
                dialog.dismiss();
            }
        })  /*{
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("header1", "header1");
                return headers;
            }
        }*/
                ;jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    private void saveToSession(String id, String nama, String loginEmail) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("sesiLoginMcs_id", id);
        editor.putString("sesiLoginMcs_nama", nama);
        editor.putString("sesiLoginMcs_email", loginEmail);
        editor.commit();
        /*Intent b = new Intent(LoginActivity.this,HomeActivity.class);
        startActivity(b);
        finish();*/
        Intent b = new Intent(LoginActivity.this,InfoAwalActivity.class);
        startActivity(b);
        finish();
    }


}
