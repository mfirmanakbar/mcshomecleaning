package mcs.mitratama.com.mcshomecleaning.data;

/**
 * Created by koko on 7/10/16.
 */
public class dataSqliteRoom {
    private String id, id_room, nama;
    private int waktuestimasi, totalroom, totalbayar;

    public dataSqliteRoom() {
    }

    public dataSqliteRoom(String id, String id_room, String nama, int waktuestimasi, int totalroom, int totalbayar) {
        this.id = id;
        this.id_room = id_room;
        this.nama = nama;
        this.waktuestimasi = waktuestimasi;
        this.totalroom = totalroom;
        this.totalbayar = totalbayar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_room() {
        return id_room;
    }

    public void setId_room(String id_room) {
        this.id_room = id_room;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getWaktuestimasi() {
        return waktuestimasi;
    }

    public void setWaktuestimasi(int waktuestimasi) {
        this.waktuestimasi = waktuestimasi;
    }

    public int getTotalroom() {
        return totalroom;
    }

    public void setTotalroom(int totalroom) {
        this.totalroom = totalroom;
    }

    public int getTotalbayar() {
        return totalbayar;
    }

    public void setTotalbayar(int totalbayar) {
        this.totalbayar = totalbayar;
    }
}
