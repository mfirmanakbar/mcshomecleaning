package mcs.mitratama.com.mcshomecleaning;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import mcs.mitratama.com.mcshomecleaning.adapter.MyPagerAdapter;
import mcs.mitratama.com.mcshomecleaning.fragment.HistoryHistoriesActivity;
import mcs.mitratama.com.mcshomecleaning.fragment.HistoryOrderActivity;

public class HistoriesActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener  {

    ViewPager vp;
    TabLayout tp;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        /*view pager*/
        vp = (ViewPager)findViewById(R.id.mViewpager_ID);
        this.addPages();

        /*tab layout*/
        tp = (TabLayout)findViewById(R.id.mTab_ID);
        tp.setTabGravity(TabLayout.GRAVITY_FILL);
        tp.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tp.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPutih));
        tp.setupWithViewPager(vp);
        tp.setOnTabSelectedListener(this);

    }

    private void addPages() {
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(this.getSupportFragmentManager());
        pagerAdapter.addFragment(new HistoryOrderActivity());
        pagerAdapter.addFragment(new HistoryHistoriesActivity());
        vp.setAdapter(pagerAdapter);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        vp.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
