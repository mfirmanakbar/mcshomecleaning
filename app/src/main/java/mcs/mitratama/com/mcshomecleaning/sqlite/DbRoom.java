package mcs.mitratama.com.mcshomecleaning.sqlite;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbRoom {
	private SQLiteDatabase db;
	private final Context con;
	private final DbOpenHelper dbHelper;

	public DbRoom(Context con) {
		this.con = con;
		dbHelper = new DbOpenHelper(this.con, "", null, 0);
	}
	
	public void open() {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		db.close();
	}

	public long insertRoom(String id_room, String nama, int waktuestimasi, int totalroom, int totalbayar) {
		ContentValues newValues = new ContentValues();
		newValues.put("id_room", id_room);
		newValues.put("nama", nama);
		newValues.put("waktuestimasi", waktuestimasi);
		newValues.put("totalroom", totalroom);
		newValues.put("totalbayar", totalbayar);
		return db.insert("tb_room", null, newValues);
	}

	public Cursor getAllRoom(){
		return db.rawQuery("SELECT * FROM tb_room ORDER BY id ASC", null);
	}

	public long deleteRoom(String id) {
		return db.delete("tb_room", "id = " + id, null);
	}

	public long deleteAllRoom() {
		return db.delete("tb_room", null, null);
	}

	public Cursor sumWaktuEstimasi(){
		return db.rawQuery("select sum(waktuestimasi) from tb_room", null);
	}
	public Cursor countTbRoom(){
		return db.rawQuery("select count(*) from tb_room", null);
	}
	public Cursor sumTotalRoom(){
		return db.rawQuery("select sum(totalroom) from tb_room", null);
	}
	public Cursor sumTotalBayar(){
		return db.rawQuery("select sum(totalbayar) from tb_room", null);
	}

}
