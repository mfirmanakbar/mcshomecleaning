package mcs.mitratama.com.mcshomecleaning;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

public class DetailContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contact);

        LinearLayout ContactUsLinearWww = (LinearLayout)findViewById(R.id.ContactUsLinearWww);
        LinearLayout ContactUsLinearEmail = (LinearLayout)findViewById(R.id.ContactUsLinearEmail);
        LinearLayout ContactUsLinearCallMobile = (LinearLayout)findViewById(R.id.ContactUsLinearCallMobile);
        LinearLayout ContactUsLinearCallOffice = (LinearLayout)findViewById(R.id.ContactUsLinearCallOffice);

        ContactUsLinearWww.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://www.ptmitratama.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        ContactUsLinearEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                //Uri data = Uri.parse("mailto:?subject=" + subject + "&body=" + body);
                Uri data = Uri.parse("mailto:?subject=Message&body=");
                intent.setData(data);
                startActivity(intent);
            }
        });

        ContactUsLinearCallMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+6282298613770"));
                try {
                    startActivity(in);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Your activity is not founded!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ContactUsLinearCallOffice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:02127650208"));
                try {
                    startActivity(in);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Your activity is not founded!", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
