package mcs.mitratama.com.mcshomecleaning;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mcs.mitratama.com.mcshomecleaning.adapter.AdapterSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.sqlite.DbRoom;
import mcs.mitratama.com.mcshomecleaning.util.Server;

public class OrderActivity extends AppCompatActivity {

    ListView list;
    AdapterSqliteRoom adapter;
    List<dataSqliteRoom> itemList = new ArrayList<dataSqliteRoom>();

    TextView txtOrderLokasiSaya, txtOrderAddressDetail, txtOrderDate, txtOrderTime, txtOrderNote, txtOrderTotalBayar, txtOrderInvoice, txtOrderAtmInfo, orderTerm1, orderTerm2;
    Button btnOrderFinish;
    LinearLayout setNote;
    CheckBox checkBoxOrderTerm;
    Spinner spinnerOrderGender;

    List<String> listCleaner;
    ArrayAdapter<String> dataAdapterGender;

    Context mContext=this;

    private JSONObject jsonObject;
    private ProgressDialog dialog = null;

    public static String sumTotalBayarx, sumTotalRoomx, sumTotalEstiamsiWaktux;
    public static int countTbRoom;

    String gender_cleaner;

    SharedPreferences sp;

    public static Activity OrderActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        OrderActivity = OrderActivity.this;

        sp = getSharedPreferences("sesiLoginMcs", Context.MODE_PRIVATE);

        list    = (ListView) findViewById(R.id.listViewOrderDetail);
        adapter = new AdapterSqliteRoom(OrderActivity.this, (ArrayList<dataSqliteRoom>) itemList);
        list.setAdapter(adapter);

        txtOrderLokasiSaya = (TextView)findViewById(R.id.txtOrderLokasiSaya);
        txtOrderAddressDetail = (TextView)findViewById(R.id.txtOrderAddressDetail);
        txtOrderDate = (TextView)findViewById(R.id.txtOrderDate);
        txtOrderTime = (TextView)findViewById(R.id.txtOrderTime);
        txtOrderNote = (TextView)findViewById(R.id.txtOrderNote);
        txtOrderTotalBayar = (TextView)findViewById(R.id.txtOrderTotalBayar);
        btnOrderFinish = (Button)findViewById(R.id.btnOrderFinish);
        setNote = (LinearLayout)findViewById(R.id.setNote);
        txtOrderInvoice = (TextView)findViewById(R.id.txtOrderInvoice);
        txtOrderAtmInfo = (TextView)findViewById(R.id.txtOrderAtmInfo);

        checkBoxOrderTerm = (CheckBox) findViewById(R.id.checkBoxOrderTerm);
        orderTerm1 = (TextView)findViewById(R.id.orderTerm1);
        orderTerm2 = (TextView)findViewById(R.id.orderTerm2);

        spinnerOrderGender = (Spinner) findViewById(R.id.spinnerOrderGender);
        addGenderCleaner();

        txtOrderLokasiSaya.setText(HomeActivity.doneAddresstrue);
        txtOrderAddressDetail.setText(HomeActivity.doneDetailAddresstrue);
        txtOrderDate.setText(HomeActivity.doneDatetrue);
        txtOrderTime.setText(HomeActivity.doneTimetrue);

        if(RoomActivity.donePaymentTypeTrue.equals("Transfer")){
            txtOrderAtmInfo.setVisibility(View.VISIBLE);
        }

        setNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(OrderActivity.this, NoteActivity.class);
                startActivity(a);
            }
        });

        orderTerm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(OrderActivity.this,Term2Activity.class);
                startActivity(a);
            }
        });
        orderTerm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(OrderActivity.this,Term2Activity.class);
                startActivity(a);
            }
        });

        sumSemuanya();

        btnOrderFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender_cleaner = spinnerOrderGender.getSelectedItem().toString();
                if(gender_cleaner.equals("Choose Cleaner")){
                    Toast.makeText(getApplication(),"Choose Cleaner Gender!",Toast.LENGTH_LONG).show();
                }else if(checkBoxOrderTerm.isChecked()!=true){
                    Toast.makeText(getApplication(),"Check Term of Use and Privacy police!",Toast.LENGTH_LONG).show();
                }
                else{
                    orderNow(HomeActivity.doneDatefalse, HomeActivity.doneTimetrue, HomeActivity.doneLattrue,
                            HomeActivity.doneLongtrue, HomeActivity.doneAddresstrue, HomeActivity.doneDetailAddresstrue,
                            sumTotalEstiamsiWaktux, sumTotalBayarx,
                            sumTotalRoomx, RoomActivity.donePaymentTypeTrue, txtOrderNote.getText().toString(),
                            HomeActivity.doneIdUsertrue, gender_cleaner);
                }
            }
        });

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading Order...");
        dialog.setCancelable(false);

    }

    private void addGenderCleaner() {
        listCleaner = new ArrayList<String>();
        listCleaner.add("Choose Cleaner");
        listCleaner.add("Male");
        listCleaner.add("Female");
        dataAdapterGender = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, listCleaner);
        dataAdapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOrderGender.setAdapter(dataAdapterGender);
    }

    private void orderNow(final String doneDatefalse, final String doneTimetrue, final double doneLattrue,
                          final double doneLongtrue, final String doneAddresstrue, final String doneDetailAddresstrue,
                          final String sumTotalEstiamsiWaktux, final String sumTotalBayarx,
                          final String sumTotalRoomx, final String donePaymentTypeTrue,
                          final String txtOrderNote, final String doneIdUsertrue, final String gender_cleaner) {

        jsonObject = new JSONObject();
        dialog.show();
        try {
            jsonObject.put("id_user", doneIdUsertrue);
            jsonObject.put("dated", doneDatefalse);
            jsonObject.put("timed", doneTimetrue);
            jsonObject.put("lat", doneLattrue);
            jsonObject.put("lot", doneLongtrue);
            jsonObject.put("detail_alamat", doneDetailAddresstrue);
            jsonObject.put("alamat", doneAddresstrue);
            jsonObject.put("total_pembayaran", sumTotalBayarx);
            jsonObject.put("total_estimasi_waktu", sumTotalEstiamsiWaktux.trim());
            jsonObject.put("total_jumlah_room", sumTotalRoomx.trim());
            jsonObject.put("note", txtOrderNote.trim());
            jsonObject.put("type_payment", donePaymentTypeTrue.trim());
            jsonObject.put("gender_cleaner", gender_cleaner);

            Log.e("orderNow", doneIdUsertrue + "\n" + doneDatefalse + "\n" + doneTimetrue + "\n" + doneLattrue + "\n" + doneLongtrue +
                    "\n" + doneDetailAddresstrue + "\n" + doneAddresstrue + "\n" + sumTotalBayarx + "\n"
                    + sumTotalEstiamsiWaktux + "\n" + sumTotalRoomx + "\n" + txtOrderNote + "\n" +
                    donePaymentTypeTrue + "\n" + gender_cleaner);

        } catch (JSONException e) {
            Log.e("orderNow", e.toString());
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.order, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("orderNow", jsonObject.toString());
                        try {
                            Boolean success = jsonObject.getBoolean("success");
                            String info = jsonObject.getString("info");
                            String id_invoice = jsonObject.getString("id_invoice");
                            Log.e("orderNow", success + ", " + info + ", " + id_invoice);

                            if(success==true){
                                Intent aa = new Intent(OrderActivity.this,DoneActivity.class);
                                aa.putExtra("id_invoice",id_invoice);
                                startActivity(aa);
                            }else {
                                Toast.makeText(getApplication(), "Order Failed!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
            }
        })


        /*{
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                *//*headers.put("token-api", sp.getString("sesiLoginMcs_token_api", null));
                headers.put("session-id", sp.getString("sesiLoginMcs_session_id", null));*//*
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token-api", "da39a3ee5e6b4b0d3255bfef95601890afd80709");
                headers.put("session-id", "e9628f423045385b7e2e8e43d697c08f");
                //Log.e("orderNow", sp.getString("sesiLoginMcs_token_api", null)+", "+sp.getString("sesiLoginMcs_session_id", null));
                return headers;
            }
        }*/

                ;jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }


    private void sumSemuanya() {
        //total isi tabel
        DbRoom dbx = new DbRoom(OrderActivity.this);
        dbx.open();
        Cursor curx = dbx.countTbRoom();
        curx.moveToFirst();
        countTbRoom = curx.getInt(0);
        dbx.close();

        //sum total pembayaran
        DbRoom db = new DbRoom(OrderActivity.this);
        db.open();
        Cursor cur = db.sumTotalBayar();
        cur.moveToFirst();
        int sumTotalBayar = cur.getInt(0);
        db.close();

        //sum total room
        DbRoom db2 = new DbRoom(OrderActivity.this);
        db2.open();
        Cursor cur2 = db2.sumTotalRoom();
        cur2.moveToFirst();
        int sumTotalRoom = cur2.getInt(0);
        db2.close();

        //sum total waktu estimasi
        DbRoom db3 = new DbRoom(OrderActivity.this);
        db3.open();
        Cursor cur3 = db3.sumWaktuEstimasi();
        cur3.moveToFirst();
        int sumTotalEstiamsiWaktu = cur3.getInt(0);
        db3.close();

        Log.d("sum tb tr tw", String.valueOf(sumTotalBayar + "-" + sumTotalRoom + "-" + sumTotalEstiamsiWaktu));

        sumTotalBayarx = String.valueOf(sumTotalBayar);
        sumTotalRoomx = String.valueOf(sumTotalRoom);
        sumTotalEstiamsiWaktux = String.valueOf(sumTotalEstiamsiWaktu);

        txtOrderTotalBayar.setText(sumTotalRoom+" rooms"+", "+sumTotalEstiamsiWaktu+" minutes. "+ RoomActivity.donePaymentTypeTrue +": "+ "Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(String.valueOf(sumTotalBayar))));
    }

    @Override
    protected void onResume() {
        txtOrderNote.setText(NoteActivity.note.toString());
        itemList.clear();
        loadRooms();
        super.onResume();
    }

    private void loadRooms() {

        DbRoom db = new DbRoom(OrderActivity.this);
        db.open();
        Cursor cur = db.getAllRoom();
        if(cur.getCount()!=0){
            DbRoom db2 = new DbRoom(OrderActivity.this);
            db2.open();
            Cursor cur2 = db.getAllRoom();
            if (cur2 != null) {
                if (cur2.moveToFirst()) {
                    if (cur2.getCount() != 0) {

                        do {

                            dataSqliteRoom item = new dataSqliteRoom();
                            item.setId(cur2.getString(0));
                            item.setId_room(cur2.getString(1));
                            item.setNama(cur2.getString(2));
                            item.setWaktuestimasi(cur2.getInt(3));
                            item.setTotalroom(cur2.getInt(4));
                            item.setTotalbayar(cur2.getInt(5));
                            itemList.add(item);

                        } while (cur2.moveToNext());
                    } else {}
                }
            } else {}
            db.close();
        }else {}
        db.close();
    }
}
