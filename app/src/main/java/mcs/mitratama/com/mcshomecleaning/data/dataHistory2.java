package mcs.mitratama.com.mcshomecleaning.data;

/**
 * Created by koko on 7/13/16.
 */
public class dataHistory2 {

    private String id2,id_invoice2,id_user2,id_pekerja2,dated2,timed2,lat2,lot2,alamat2,detail_alamat2,total_pembayaran2,total_estimasi_waktu2,total_jumlah_room2,flag2,note2,type_payment2,review2,review_detail2,post_date2, nama_pekerja2, gender_cleaner2;

    public dataHistory2() {
    }

    public dataHistory2(String id2, String id_invoice2, String id_user2, String id_pekerja2, String dated2, String timed2, String lat2, String lot2, String alamat2, String detail_alamat2, String total_pembayaran2, String total_estimasi_waktu2, String total_jumlah_room2, String flag2, String note2, String type_payment2, String review2, String review_detail2, String post_date2, String nama_pekerja2, String gender_cleaner2) {
        this.id2 = id2;
        this.id_invoice2 = id_invoice2;
        this.id_user2 = id_user2;
        this.id_pekerja2 = id_pekerja2;
        this.dated2 = dated2;
        this.timed2 = timed2;
        this.lat2 = lat2;
        this.lot2 = lot2;
        this.alamat2 = alamat2;
        this.detail_alamat2 = detail_alamat2;
        this.total_pembayaran2 = total_pembayaran2;
        this.total_estimasi_waktu2 = total_estimasi_waktu2;
        this.total_jumlah_room2 = total_jumlah_room2;
        this.flag2 = flag2;
        this.note2 = note2;
        this.type_payment2 = type_payment2;
        this.review2 = review2;
        this.review_detail2 = review_detail2;
        this.post_date2 = post_date2;
        this.nama_pekerja2 = nama_pekerja2;
        this.gender_cleaner2 = gender_cleaner2;
    }

    public String getNama_pekerja2() {
        return nama_pekerja2;
    }

    public void setNama_pekerja2(String nama_pekerja2) {
        this.nama_pekerja2 = nama_pekerja2;
    }

    public String getGender_cleaner2() {
        return gender_cleaner2;
    }

    public void setGender_cleaner2(String gender_cleaner2) {
        this.gender_cleaner2 = gender_cleaner2;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getId_invoice2() {
        return id_invoice2;
    }

    public void setId_invoice2(String id_invoice2) {
        this.id_invoice2 = id_invoice2;
    }

    public String getId_user2() {
        return id_user2;
    }

    public void setId_user2(String id_user2) {
        this.id_user2 = id_user2;
    }

    public String getId_pekerja2() {
        return id_pekerja2;
    }

    public void setId_pekerja2(String id_pekerja2) {
        this.id_pekerja2 = id_pekerja2;
    }

    public String getDated2() {
        return dated2;
    }

    public void setDated2(String dated2) {
        this.dated2 = dated2;
    }

    public String getTimed2() {
        return timed2;
    }

    public void setTimed2(String timed2) {
        this.timed2 = timed2;
    }

    public String getLat2() {
        return lat2;
    }

    public void setLat2(String lat2) {
        this.lat2 = lat2;
    }

    public String getLot2() {
        return lot2;
    }

    public void setLot2(String lot2) {
        this.lot2 = lot2;
    }

    public String getAlamat2() {
        return alamat2;
    }

    public void setAlamat2(String alamat2) {
        this.alamat2 = alamat2;
    }

    public String getDetail_alamat2() {
        return detail_alamat2;
    }

    public void setDetail_alamat2(String detail_alamat2) {
        this.detail_alamat2 = detail_alamat2;
    }

    public String getTotal_pembayaran2() {
        return total_pembayaran2;
    }

    public void setTotal_pembayaran2(String total_pembayaran2) {
        this.total_pembayaran2 = total_pembayaran2;
    }

    public String getTotal_estimasi_waktu2() {
        return total_estimasi_waktu2;
    }

    public void setTotal_estimasi_waktu2(String total_estimasi_waktu2) {
        this.total_estimasi_waktu2 = total_estimasi_waktu2;
    }

    public String getTotal_jumlah_room2() {
        return total_jumlah_room2;
    }

    public void setTotal_jumlah_room2(String total_jumlah_room2) {
        this.total_jumlah_room2 = total_jumlah_room2;
    }

    public String getFlag2() {
        return flag2;
    }

    public void setFlag2(String flag2) {
        this.flag2 = flag2;
    }

    public String getNote2() {
        return note2;
    }

    public void setNote2(String note2) {
        this.note2 = note2;
    }

    public String getType_payment2() {
        return type_payment2;
    }

    public void setType_payment2(String type_payment2) {
        this.type_payment2 = type_payment2;
    }

    public String getReview2() {
        return review2;
    }

    public void setReview2(String review2) {
        this.review2 = review2;
    }

    public String getReview_detail2() {
        return review_detail2;
    }

    public void setReview_detail2(String review_detail2) {
        this.review_detail2 = review_detail2;
    }

    public String getPost_date2() {
        return post_date2;
    }

    public void setPost_date2(String post_date2) {
        this.post_date2 = post_date2;
    }
}
