package mcs.mitratama.com.mcshomecleaning;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import mcs.mitratama.com.mcshomecleaning.util.Server;

public class RegistrationActivity extends AppCompatActivity {

    Spinner spinnerGender;
    FloatingActionButton fab;
    ArrayAdapter<String> dataAdapter;
    EditText txtRegistName,txtRegistEmail,txtRegistPhoneNumber,txtRegistPassword,txtRegistPasswordConfirm;
    CheckBox checkBoxRegistTerm;
    Button btnRegistRegister;
    String device_id;
    TelephonyManager tm;
    private JSONObject jsonObject;
    private ProgressDialog dialog = null;
    Boolean success;
    String info;
    TextView registerTerm1, registerTerm2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        spinnerGender = (Spinner) findViewById(R.id.spinnerRegistSex);
        txtRegistName = (EditText) findViewById(R.id.txtRegistName);
        txtRegistEmail = (EditText) findViewById(R.id.txtRegistEmail);
        txtRegistPhoneNumber = (EditText) findViewById(R.id.txtRegistPhoneNumber);
        txtRegistPassword = (EditText) findViewById(R.id.txtRegistPassword);
        txtRegistPasswordConfirm = (EditText) findViewById(R.id.txtRegistPasswordConfirm);
        checkBoxRegistTerm = (CheckBox) findViewById(R.id.checkBoxRegistTerm);
        btnRegistRegister = (Button) findViewById(R.id.btnRegistRegister);
        registerTerm1 = (TextView)findViewById(R.id.registerTerm1);
        registerTerm2 = (TextView)findViewById(R.id.registerTerm2);

        addGender();

        tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();

        btnRegistRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PendaftaranUser();
            }
        });

        registerTerm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(RegistrationActivity.this,Term2Activity.class);
                startActivity(a);
            }
        });
        registerTerm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(RegistrationActivity.this,Term2Activity.class);
                startActivity(a);
            }
        });

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading Registration...");
        dialog.setCancelable(false);

        jsonObject = new JSONObject();
    }

    private boolean isValidEmaillId(String email){
        return Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches();
    }

    private void PendaftaranUser() {
        String regisName = txtRegistName.getText().toString();
        String regisEmail = txtRegistEmail.getText().toString();
        String regisPhone = txtRegistPhoneNumber.getText().toString();
        String regisPassword = txtRegistPassword.getText().toString();
        String regisPasswordConfirm = txtRegistPasswordConfirm.getText().toString();

        if(regisName.length() < 2){
            txtRegistName.setError("Please input your name!");Toast.makeText(getApplication(),"Please input your Name!",Toast.LENGTH_LONG).show();
        }else if(regisEmail.length() == 0){
            txtRegistEmail.setError("Please input your email!");Toast.makeText(getApplication(),"Please input your Email!",Toast.LENGTH_LONG).show();
        }else if(isValidEmaillId(txtRegistEmail.getText().toString().trim())==false){
            txtRegistEmail.setError("Invalid email address!");Toast.makeText(getApplication(),"Invalid email address!",Toast.LENGTH_LONG).show();
        }else if(regisPhone.length() < 5){
            txtRegistPhoneNumber.setError("Please input your Phone Number!");Toast.makeText(getApplication(),"Please input your Phone Number!",Toast.LENGTH_LONG).show();
        }else if(regisPassword.length() == 0){
            txtRegistPassword.setError("Please input your Password!");Toast.makeText(getApplication(),"Please input your Password!",Toast.LENGTH_LONG).show();
        }else if(regisPassword.length() > 0 && regisPassword.length() < 5){
            txtRegistPassword.setError("Password must be 5 Character!");Toast.makeText(getApplication(),"Password must be 5 Character!",Toast.LENGTH_LONG).show();
        }else if(regisPasswordConfirm.length() == 0){
            txtRegistPasswordConfirm.setError("Please input your Password Confirm!");Toast.makeText(getApplication(),"Please input your Password Confirm!",Toast.LENGTH_LONG).show();
        }else if(regisPasswordConfirm.length() > 0 && regisPasswordConfirm.length() < 5){
            txtRegistPasswordConfirm.setError("Password Confirm must be 5 Character!");Toast.makeText(getApplication(),"Password Confirm must be 5 Character!",Toast.LENGTH_LONG).show();
        }else if(!TextUtils.equals(txtRegistPassword.getText(),txtRegistPasswordConfirm.getText())){
            Toast.makeText(getApplication(),"Password Confirm doesn't match! ",Toast.LENGTH_LONG).show();
        }else if(checkBoxRegistTerm.isChecked()!=true){
            Toast.makeText(getApplication(),"Check Term of Use and Privacy police!",Toast.LENGTH_LONG).show();
        }else{
            okeSaveIt(regisName,String.valueOf(spinnerGender.getSelectedItem()),regisEmail,regisPhone,regisPassword, device_id);
        }
    }

    private void okeSaveIt(final String regisName, final String regisGender, final String regisEmail, final String regisPhone, final String regisPassword, final String device_id) {
        //Toast.makeText(getApplication(),regisName+", "+regisGender+", "+regisEmail+", "+regisPhone+", "+regisPassword+", "+device_id,Toast.LENGTH_LONG).show();
        dialog.show();
        try {
            jsonObject.put("nama", regisName.trim());
            jsonObject.put("sex", regisGender.trim());
            jsonObject.put("email", regisEmail.trim());
            jsonObject.put("no_handphone", regisPhone.trim());
            jsonObject.put("password", regisPassword.trim());
            jsonObject.put("imei", device_id.trim());
            Log.d("okeSaveIt",regisName.trim()+", "+regisGender.trim()+", "+regisEmail.trim()+", "+regisPhone.trim()+", "+regisPassword.trim()+", "+device_id.trim());
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        }JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.resiter_user, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("okeSaveIt", jsonObject.toString());
                        dialog.dismiss();
                        try {
                            success = jsonObject.getBoolean("success");
                            info = jsonObject.getString("info");
                            Log.e("okeSaveIt", success+", "+info);
                            if(success==true){
                                Toast.makeText(getApplication(), "Registration Successful!", Toast.LENGTH_SHORT).show();
                                finish();
                            }else {
                                Toast.makeText(getApplication(), "Failed!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
                dialog.dismiss();
            }
        });jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    public void addGender() {
        List<String> list = new ArrayList<String>();
        list.add("Male");
        list.add("Female");
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(dataAdapter);
    }

}
