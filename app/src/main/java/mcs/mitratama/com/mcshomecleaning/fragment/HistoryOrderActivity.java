package mcs.mitratama.com.mcshomecleaning.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mcs.mitratama.com.mcshomecleaning.HistoryDetailActivity;
import mcs.mitratama.com.mcshomecleaning.HomeActivity;
import mcs.mitratama.com.mcshomecleaning.R;
import mcs.mitratama.com.mcshomecleaning.adapter.HistoryAdapter;
import mcs.mitratama.com.mcshomecleaning.data.dataHistory;
import mcs.mitratama.com.mcshomecleaning.util.Server;

public class HistoryOrderActivity extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = HistoryOrderActivity.class.getSimpleName();
    ListView list;
    SwipeRefreshLayout swipe;
    List<dataHistory> itemList = new ArrayList<dataHistory>();
    HistoryAdapter adapter;
    Context c;
    JSONObject jsonObject;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_history_order,container,false);

        swipe   = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh_history_order);
        list    = (ListView)rootView.findViewById(R.id.listView_history_order);

        adapter = new HistoryAdapter(this.getActivity(), (ArrayList<dataHistory>) itemList);
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(this);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {

                final String xid = itemList.get(position).getId();
                final String xid_invoice = itemList.get(position).getId_invoice();
                final String xid_user = itemList.get(position).getId_user();
                final String xid_pekerja = itemList.get(position).getId_pekerja();
                final String xdated = itemList.get(position).getDated();
                final String xtimed = itemList.get(position).getTimed();
                final String xlat = itemList.get(position).getLat();
                final String xlot = itemList.get(position).getLot();
                final String xalamat = itemList.get(position).getAlamat();
                final String xdetail_alamat = itemList.get(position).getDetail_alamat();
                final String xtotal_pembayaran = itemList.get(position).getTotal_pembayaran();
                final String xtotal_estimasi_waktu = itemList.get(position).getTotal_estimasi_waktu();
                final String xtotal_jumlah_room = itemList.get(position).getTotal_jumlah_room();
                final String xflag = itemList.get(position).getFlag();
                final String xnote = itemList.get(position).getNote();
                final String xtype_payment = itemList.get(position).getType_payment();
                final String xreview = itemList.get(position).getReview();
                final String xreview_detail = itemList.get(position).getReview_detail();
                final String xpost_date = itemList.get(position).getPost_date();
                final String xnama_pekerja = itemList.get(position).getNama_pekerja();
                final String xgender_cleaner = itemList.get(position).getGender_cleaner();

                Intent aa = new Intent(getContext(),HistoryDetailActivity.class);
                aa.putExtra("xid",xid);
                aa.putExtra("xid_invoice",xid_invoice);
                aa.putExtra("xid_user",xid_user);
                aa.putExtra("xid_pekerja",xid_pekerja);
                aa.putExtra("xdated",xdated);
                aa.putExtra("xtimed",xtimed);
                aa.putExtra("xlat",xlat);
                aa.putExtra("xlot",xlot);
                aa.putExtra("xalamat",xalamat);
                aa.putExtra("xdetail_alamat",xdetail_alamat);
                aa.putExtra("xtotal_pembayaran",xtotal_pembayaran);
                aa.putExtra("xtotal_estimasi_waktu",xtotal_estimasi_waktu);
                aa.putExtra("xtotal_jumlah_room",xtotal_jumlah_room);
                aa.putExtra("xflag",xflag);
                aa.putExtra("xnote",xnote);
                aa.putExtra("xtype_payment",xtype_payment);
                aa.putExtra("xreview",xreview);
                aa.putExtra("xreview_detail",xreview_detail);
                aa.putExtra("xpost_date",xpost_date);
                aa.putExtra("xnama_pekerja",xnama_pekerja);
                aa.putExtra("xgender_cleaner",xgender_cleaner);
                startActivity(aa);

            }
        });

        return rootView;
    }

    @Override
    public String toString() {
        String title = "Order";
        return title;
    }

    @Override
    public void onResume() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        load_Order();
        super.onResume();
    }

    @Override
    public void onRefresh() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        load_Order();
    }

    private void load_Order() {
        swipe.setRefreshing(true);
        itemList.clear();
        adapter.notifyDataSetChanged();
        jsonObject = new JSONObject();
        try{
            final String id_user = HomeActivity.doneIdUsertrue;
            jsonObject.put("id_user", id_user);
        }catch (Exception e){
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.list_order, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{

                            Log.e("load_Order", response.toString());
                            JSONArray jsonArrayContent = response.getJSONArray("content");
                            String info = response.getString("info");
                            Boolean status = response.getBoolean("success");
                            Log.e("load_Order", String.valueOf(jsonArrayContent));
                            Log.e("load_Order", info);
                            Log.e("load_Order", String.valueOf(status));

                            if(status==true){
                                Log.e("load_Order", "Yes1");
                                 for(int i=0; i<jsonArrayContent.length();i++){

                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String id = jsonObjectContent.getString("id");
                                    String id_invoice = jsonObjectContent.getString("id_invoice");
                                    String id_user = jsonObjectContent.getString("id_user");
                                    String id_pekerja = jsonObjectContent.getString("id_pekerja");
                                    String dated = jsonObjectContent.getString("dated");
                                    String timed = jsonObjectContent.getString("timed");
                                    String lat = jsonObjectContent.getString("lat");
                                    String lot = jsonObjectContent.getString("lot");
                                    String alamat = jsonObjectContent.getString("alamat");
                                    String detail_alamat = jsonObjectContent.getString("detail_alamat");
                                    String total_pembayaran = jsonObjectContent.getString("total_pembayaran");
                                    String total_estimasi_waktu = jsonObjectContent.getString("total_estimasi_waktu");
                                    String total_jumlah_room = jsonObjectContent.getString("total_jumlah_room");
                                    String flag = jsonObjectContent.getString("flag");
                                    String note = jsonObjectContent.getString("note");
                                    String type_payment = jsonObjectContent.getString("type_payment");
                                    String review = jsonObjectContent.getString("review");
                                    String review_detail = jsonObjectContent.getString("review_detail");
                                    String post_date = jsonObjectContent.getString("post_date");
                                    String nama_pekerja = jsonObjectContent.getString("nama_pekerja");
                                    String gender_cleaner = jsonObjectContent.getString("gender_cleaner");

                                    //Log.e("load_Order", "idnya= "+id);

                                    dataHistory item = new dataHistory();
                                    item.setId(id);
                                    item.setId_invoice(id_invoice);
                                    item.setId_user(id_user);
                                    item.setId_pekerja(id_pekerja);
                                    item.setDated(dated);
                                    item.setTimed(timed);
                                    item.setLat(lat);
                                    item.setLot(lot);
                                    item.setAlamat(alamat);
                                    item.setDetail_alamat(detail_alamat);
                                    item.setTotal_pembayaran(total_pembayaran);
                                    item.setTotal_estimasi_waktu(total_estimasi_waktu);
                                    item.setTotal_jumlah_room(total_jumlah_room);
                                    item.setFlag(flag);
                                    item.setNote(note);
                                    item.setType_payment(type_payment);
                                    item.setReview(review);
                                    item.setReview_detail(review_detail);
                                    item.setPost_date(post_date);
                                    item.setNama_pekerja(nama_pekerja);
                                    item.setGender_cleaner(gender_cleaner);
                                    itemList.add(item);

                                }
                            }else{
                                Toast.makeText(c,"Failed!",Toast.LENGTH_SHORT).show();
                            }
                        }
                        catch (JSONException e){
                            Log.e("error", e.getMessage());
                        }
                        adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage(), TAG);
                swipe.setRefreshing(false);
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getContext()).add(jsonObjectRequest);

    }
}
