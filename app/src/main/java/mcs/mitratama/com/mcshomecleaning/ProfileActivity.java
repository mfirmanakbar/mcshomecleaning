package mcs.mitratama.com.mcshomecleaning;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mcs.mitratama.com.mcshomecleaning.util.Server;

public class ProfileActivity extends AppCompatActivity {

    Spinner spinnerProfileSex;
    ArrayAdapter<String> dataAdapter;
    EditText txtProfileName,txtProfilePhoneNumber,txtProfileOldPassword,txtProfileNewPassword;
    TextView txtProfileEmail, txtProfileGender;
    Button btnProfileUpdate;
    SharedPreferences sp;
    private JSONObject jsonObject;
    Boolean success;
    String info;
    List<String> list;

    private JSONObject jsonObject2;
    private ProgressDialog dialog2 = null;
    Boolean success2;
    String info2;

    private JSONObject jsonObject3;
    private ProgressDialog dialog3 = null;
    Boolean success3;
    String info3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        spinnerProfileSex = (Spinner) findViewById(R.id.spinnerProfileSex);
        addGender();

        dialog2 = new ProgressDialog(this);
        dialog2.setMessage("Chek old password ...");
        dialog2.setCancelable(false);

        dialog3 = new ProgressDialog(this);
        dialog3.setMessage("Update profile ...");
        dialog3.setCancelable(false);

        txtProfileEmail = (TextView)findViewById(R.id.txtProfileEmail);
        txtProfileGender = (TextView)findViewById(R.id.txtProfileGender);
        txtProfileName = (EditText)findViewById(R.id.txtProfileName);
        txtProfilePhoneNumber = (EditText)findViewById(R.id.txtProfilePhoneNumber);
        txtProfileOldPassword = (EditText)findViewById(R.id.txtProfileOldPassword);
        txtProfileNewPassword = (EditText)findViewById(R.id.txtProfileNewPassword);
        btnProfileUpdate = (Button)findViewById(R.id.btnProfileUpdate);

        sp = getSharedPreferences("sesiLoginMcs", Context.MODE_PRIVATE);
        String spEmail = sp.getString("sesiLoginMcs_email", null);
        loadMyProfile(spEmail);

        btnProfileUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtProfileName.getText().toString().equals("")){
                    Toast.makeText(getApplication(),"Name is null!",Toast.LENGTH_SHORT).show();
                }else if(spinnerProfileSex.getSelectedItem().toString().equals("Choose Gender")){
                    Toast.makeText(getApplication(),"Gender is null!",Toast.LENGTH_SHORT).show();
                }else if(txtProfilePhoneNumber.getText().toString().equals("")){
                    Toast.makeText(getApplication(),"Phone Number is null!",Toast.LENGTH_SHORT).show();
                }else if(txtProfileOldPassword.length() < 5){
                    Toast.makeText(getApplication(),"Old Password must be 5 character!",Toast.LENGTH_SHORT).show();
                }else if(txtProfileNewPassword.length() < 5){
                    Toast.makeText(getApplication(),"New Password must be 5 character!",Toast.LENGTH_SHORT).show();
                }else{
                    okeSave(txtProfileEmail.getText().toString(),txtProfileOldPassword.getText().toString());
                }
            }
        });
    }

    private void okeSave(final String email, final String oldpassword) {
        jsonObject2 = new JSONObject();
        dialog2.show();
        try {
            jsonObject2.put("email", email.trim());
            jsonObject2.put("password", oldpassword.trim());
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.profile_cek,
                jsonObject2, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        dialog2.dismiss();
                        try {
                            success2 = jsonObject.getBoolean("success");
                            info2 = jsonObject.getString("info");
                            Log.e("okeSaveIt", success2+", "+info2);
                            if(success2==true){
                                Toast.makeText(getApplication(), "Successful! [1]", Toast.LENGTH_SHORT).show();
                                update_now(email, txtProfileName.getText().toString(), spinnerProfileSex.getSelectedItem().toString(), txtProfilePhoneNumber.getText().toString(), txtProfileNewPassword.getText().toString());
                                finish();
                            }else {
                                Toast.makeText(getApplication(), "Failed! [1]", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
                dialog2.dismiss();
            }
        });jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    private void update_now(final String email, final String nama, final String gender, final String phone, final String newpassword) {
        jsonObject3 = new JSONObject();
        dialog3.show();
        try {
            jsonObject3.put("email", email.trim());
            jsonObject3.put("password", newpassword.trim());
            jsonObject3.put("nama", nama.trim());
            jsonObject3.put("sex", gender.trim());
            jsonObject3.put("no_handphone", phone.trim());
            Log.e("un",email+newpassword+nama+gender+phone);
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.profile_update,
                jsonObject3, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                dialog3.dismiss();

                try {
                    success3 = jsonObject.getBoolean("success");
                    info3 = jsonObject.getString("info");
                    Log.e("okeSaveIt", success3+", "+info3);
                    if(success3==true){
                        //Toast.makeText(getApplication(), "Successful! [2]", Toast.LENGTH_SHORT).show();
                    }else {
                        //Toast.makeText(getApplication(), "Failed! [2]", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
                dialog3.dismiss();
            }
        });jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }


    private void loadMyProfile(final String spEmail) {
        jsonObject = new JSONObject();
        try {
            jsonObject.put("email", spEmail);
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        }JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.profile_load, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            Log.d("loadMyProfile", response.toString());
                            JSONArray jsonArrayContent = response.getJSONArray("content");
                            for(int i=0; i<jsonArrayContent.length();i++){
                                try {
                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String id = jsonObjectContent.getString("id");
                                    String email = jsonObjectContent.getString("email");
                                    String no_handphone = jsonObjectContent.getString("no_handphone");
                                    String nama = jsonObjectContent.getString("nama");
                                    String sex = jsonObjectContent.getString("sex");
                                    Log.d("loadMyProfile", id+email+no_handphone+nama+sex);
                                    txtProfileName.setText(nama);
                                    txtProfileEmail.setText(email);
                                    txtProfilePhoneNumber.setText(no_handphone);
                                    txtProfileGender.setText(sex);
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        }catch (JSONException e){
                            Log.e("error", e.getMessage());
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage(), getApplication());
            }
        });jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    private void addGender() {
        list = new ArrayList<String>();
        list.add("Choose Gender");
        list.add("Male");
        list.add("Female");
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProfileSex.setAdapter(dataAdapter);
    }
}
