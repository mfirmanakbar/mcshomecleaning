package mcs.mitratama.com.mcshomecleaning.data;

/**
 * Created by FIRMAN on 01-Oct-16.
 */
public class dataKategoriRoom {
    String kategori_room_id, kategori_room_nama;

    public dataKategoriRoom() {
    }

    public dataKategoriRoom(String kategori_room_id, String kategori_room_nama) {
        this.kategori_room_id = kategori_room_id;
        this.kategori_room_nama = kategori_room_nama;
    }

    public String getKategori_room_id() {
        return kategori_room_id;
    }

    public void setKategori_room_id(String kategori_room_id) {
        this.kategori_room_id = kategori_room_id;
    }

    public String getKategori_room_nama() {
        return kategori_room_nama;
    }

    public void setKategori_room_nama(String kategori_room_nama) {
        this.kategori_room_nama = kategori_room_nama;
    }
}
