package mcs.mitratama.com.mcshomecleaning;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class NoteActivity extends AppCompatActivity {

    EditText txtOrderNoteDetail;
    public static String note="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        txtOrderNoteDetail = (EditText)findViewById(R.id.txtOrderNoteDetail);
        txtOrderNoteDetail.setText(note.toString());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_done) {
            note = txtOrderNoteDetail.getText().toString();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
