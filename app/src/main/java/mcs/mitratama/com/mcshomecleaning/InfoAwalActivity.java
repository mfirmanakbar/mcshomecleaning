package mcs.mitratama.com.mcshomecleaning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InfoAwalActivity extends AppCompatActivity {

    public static String linkVal="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_awal);

        Button btnInfoAwalInfo = (Button) findViewById(R.id.btnInfoAwalInfo);
        btnInfoAwalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(InfoAwalActivity.this, HomeActivity.class);
                startActivity(b);
                finish();
            }
        });

        TextView txtViewFaq = (TextView) findViewById(R.id.txtViewFaq);
        txtViewFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linkVal = "faq";
                Intent b = new Intent(InfoAwalActivity.this, Term2Activity.class);
                startActivity(b);
            }
        });

        TextView txtViewTerms = (TextView) findViewById(R.id.txtViewTerms);
        txtViewTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linkVal = "terms";
                Intent b = new Intent(InfoAwalActivity.this, Term2Activity.class);
                startActivity(b);
            }
        });

    }
}
