package mcs.mitratama.com.mcshomecleaning;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mcs.mitratama.com.mcshomecleaning.adapter.AdapterKategoriRoom;
import mcs.mitratama.com.mcshomecleaning.adapter.AdapterSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataKategoriRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataSqliteRoom;
import mcs.mitratama.com.mcshomecleaning.sqlite.DbRoom;
import mcs.mitratama.com.mcshomecleaning.util.Server;

public class RoomActivity extends AppCompatActivity {

    FloatingActionButton fab;

    ListView list;
    AdapterSqliteRoom adapter;
    List<dataSqliteRoom> itemList = new ArrayList<dataSqliteRoom>();

    AlertDialog.Builder dialog, dialogK;
    LayoutInflater LIdialogK;
    View VdialogK;

    Button btnRoomToFinishOrder;
    public static String donePaymentTypeTrue;
    public static Activity RoomActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        donePaymentTypeTrue = "";
        RoomActivity = RoomActivity.this;

        fab = (FloatingActionButton) findViewById(R.id.fabRoom);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(RoomActivity.this, KategoriRoomActivity.class);
                startActivity(a);
                /*Intent a = new Intent(RoomActivity.this, ListRoomActivity.class);
                startActivity(a);*/
            }
        });

        btnRoomToFinishOrder = (Button)findViewById(R.id.btnRoomToFinishOrder);

        list    = (ListView) findViewById(R.id.listViewRoom_2);
        adapter = new AdapterSqliteRoom(RoomActivity.this, (ArrayList<dataSqliteRoom>) itemList);
        list.setAdapter(adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                final String xId = itemList.get(position).getId();
                final String xId_room = itemList.get(position).getId_room();
                final String xNama = itemList.get(position).getNama();
                final int xWaktuEsti = itemList.get(position).getWaktuestimasi();
                final int xTotalRoom = itemList.get(position).getTotalroom();
                final int xTotalBayar = itemList.get(position).getTotalbayar();
                //Log.d("list_room",xId+"-"+xId_room+"-"+xNama+"-"+xWaktuEsti+"-"+xTotalRoom+"-"+xTotalBayar);

                final CharSequence[] dialogitem = {"Delete " + xTotalRoom + " " + xNama};
                dialog = new AlertDialog.Builder(RoomActivity.this);
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
                                DbRoom db = new DbRoom(RoomActivity.this);
                                db.open();
                                db.deleteRoom(xId);
                                db.close();
                                Intent a = new Intent(RoomActivity.this, RoomActivity.class);
                                startActivity(a);
                                finish();
                                break;
                        }
                    }
                }).show();
            }
        });

        btnRoomToFinishOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DbRoom db = new DbRoom(RoomActivity.this);
                db.open();
                Cursor cur = db.getAllRoom();
                cur.moveToFirst();
                int count = cur.getCount();
                db.close();
                Log.d("count", String.valueOf(count));
                if (count > 0) {
                    final CharSequence[] dialogitem = {"Cash","Transfer"};
                    dialog = new AlertDialog.Builder(RoomActivity.this);
                    dialog.setCancelable(true);
                    dialog.setIcon(R.mipmap.ic_launcher);
                    dialog.setTitle("Payment Type");
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    donePaymentTypeTrue="Cash";
                                    Intent a = new Intent(RoomActivity.this, OrderActivity.class);
                                    startActivity(a);
                                    break;
                                case 1:
                                    donePaymentTypeTrue="Transfer";
                                    Intent b = new Intent(RoomActivity.this, OrderActivity.class);
                                    startActivity(b);
                                    break;
                            }
                        }
                    }).show();
                } else {
                    Toast.makeText(RoomActivity.this, "Please add the room!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //itemList.clear();
        loadRooms();
    }


    private void loadRooms() {
        DbRoom db = new DbRoom(RoomActivity.this);
        db.open();
        Cursor cur = db.getAllRoom();
        if(cur.getCount()!=0){
            DbRoom db2 = new DbRoom(RoomActivity.this);
            db2.open();
            Cursor cur2 = db.getAllRoom();
            if (cur2 != null) {
                if (cur2.moveToFirst()) {
                    if (cur2.getCount() != 0) {
                        do {
                            dataSqliteRoom item = new dataSqliteRoom();
                            item.setId(cur2.getString(0));
                            item.setId_room(cur2.getString(1));
                            item.setNama(cur2.getString(2));
                            item.setWaktuestimasi(cur2.getInt(3));
                            item.setTotalroom(cur2.getInt(4));
                            item.setTotalbayar(cur2.getInt(5));
                            itemList.add(item);
                            Log.d("sqlite_room", cur2.getString(0)+", "+cur2.getString(1)+", "+cur2.getString(2)+", "+cur2.getString(3)+", "+cur2.getString(4)+", "+cur2.getString(5));
                        } while (cur2.moveToNext());
                    } else {}
                }
            } else {}
            db.close();
        }else {}
        db.close();
    }

}
