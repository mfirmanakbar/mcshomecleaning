package mcs.mitratama.com.mcshomecleaning.util;

/**
 * Created by koko on 4/13/16.
 */
public class Server {
    public static final String iphost = "http://ptmitratama.com/mcs/api/";
    public static final String resiter_user = iphost + "login/register";
    public static final String login_user = iphost + "login";
    public static final String profile_load = iphost + "login/listMember";
    public static final String profile_cek = iphost + "login/cek_akun";
    public static final String profile_update = iphost + "login/update_profile";
    public static final String cek_email = iphost + "login/cek_email";
    public static final String forget_password = iphost + "login/forget_password";
    public static final String room_kategori = iphost + "room/area";
    public static final String room_load = iphost + "room/index/";
    public static final String order = iphost + "order";
    public static final String order_r = iphost + "order/rorder";
    public static final String list_order = iphost + "order/listorder";
    public static final String list_histories = iphost + "order/listHistory";
    public static final String list_order_r = iphost + "order/listRorder";
    public static final String order_cancel = iphost + "order/cancel";
}
