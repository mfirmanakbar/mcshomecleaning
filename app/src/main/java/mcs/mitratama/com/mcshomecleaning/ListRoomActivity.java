package mcs.mitratama.com.mcshomecleaning;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mcs.mitratama.com.mcshomecleaning.adapter.AdapterRoom;
import mcs.mitratama.com.mcshomecleaning.data.dataRoom;
import mcs.mitratama.com.mcshomecleaning.sqlite.DbRoom;
import mcs.mitratama.com.mcshomecleaning.util.Server;

public class ListRoomActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    //public static Activity jne1;
    ListView list;
    SwipeRefreshLayout swipe;
    AdapterRoom adapter;
    List<dataRoom> itemList = new ArrayList<dataRoom>();

    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    TextView add_RoomName, add_RoomHargaWaktu, add_RoomHasil;
    EditText add_RoomEstimasiWaktu, add_RoomJumlahRoom;

    String xxid, xxid_area, xxnama, xxdes, xxtime, xxharga, xxarea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_room);

        swipe   = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        list    = (ListView) findViewById(R.id.listViewListRoom);

        adapter = new AdapterRoom(ListRoomActivity.this, itemList);
        list.setAdapter(adapter);

        swipe.setOnRefreshListener(this);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                final String xid = itemList.get(position).getId();
                final String xid_area = itemList.get(position).getId_area();
                final String xnama = itemList.get(position).getNama();
                final String xdes = itemList.get(position).getDes();
                final String xtime = itemList.get(position).getTime();
                final String xharga = itemList.get(position).getHarga();
                final String xarea = itemList.get(position).getArea();
                //Log.d("list_room",xid+xid_area+xnama+xdes+xtime+xharga+xarea);
                //showInputAddRoom();
                addRoom(xid, xid_area, xnama, xdes, xtime, xharga, xarea);
                //addRoom(xid, xnama, xtime, xharga);
                //addRoom();
            }
        });

    }

    //private void addRoom() {
    private void addRoom(String xid, String xid_area, String xnama, String xdes, String xtime, String xharga, String xarea) {
    //private void addRoom(String xid, String xnama, String xtime, String xharga) {
        dialog = new AlertDialog.Builder(ListRoomActivity.this);
        inflater = this.getLayoutInflater();
        dialogView = inflater.inflate(R.layout.add_room2, null);
        dialog.setView(dialogView);

        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Add Room");

        add_RoomName = (TextView) dialogView.findViewById(R.id.add_RoomName);
        add_RoomHargaWaktu = (TextView) dialogView.findViewById(R.id.add_RoomHargaWaktu);
        add_RoomHasil = (TextView) dialogView.findViewById(R.id.add_RoomHasil);
        add_RoomEstimasiWaktu = (EditText) dialogView.findViewById(R.id.add_RoomEstimasiWaktu);
        add_RoomJumlahRoom = (EditText) dialogView.findViewById(R.id.add_RoomJumlahRoom);
        Button add_RoomBtnSaveRoom = (Button) dialogView.findViewById(R.id.add_RoomBtnSaveRoom);
        Button add_RoomBtnHitung = (Button) dialogView.findViewById(R.id.add_RoomBtnHitung);

        xxid = xid;
        xxid_area= xid_area;
        xxnama = xnama;
        xxdes = xdes;
        xxtime = xtime;
        xxharga = xharga;
        xxarea= xarea;

        //Log.d("ddd", xid + "-" + xnama + "-" + xtime + "-" + xharga);
        if(!xid.isEmpty()){
            add_RoomName.setText(xxnama);
            add_RoomHargaWaktu.setText("Rp"+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(xxharga))+" /"+xxtime+" minutes");
        }else{
            //setNUll();
        }

        add_RoomBtnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (add_RoomEstimasiWaktu.getText().toString().isEmpty()) {
                    add_RoomEstimasiWaktu.setError("Please input estimated cleaning in minutes!");
                    Toast.makeText(ListRoomActivity.this, "Please input estimated cleaning in minutes!", Toast.LENGTH_LONG).show();
                } else if (add_RoomJumlahRoom.getText().toString().isEmpty()) {
                    add_RoomJumlahRoom.setText("1");
                    Toast.makeText(ListRoomActivity.this, "Please input total room!", Toast.LENGTH_LONG).show();
                } else {
                    int esti = Integer.parseInt(add_RoomEstimasiWaktu.getText().toString());
                    int totroom = Integer.parseInt(add_RoomJumlahRoom.getText().toString());
                    int timexx = Integer.parseInt(xxtime);
                    int hargaxx = Integer.parseInt(xxharga);
                    int hasil = 0;
                    if (esti<timexx) {
                        add_RoomEstimasiWaktu.setError("Minimum should be "+xxtime+" minutes!");
                        Toast.makeText(ListRoomActivity.this, "Minimum should be "+xxtime+" minutes!", Toast.LENGTH_LONG).show();
                    } else if (totroom < 1) {
                        add_RoomJumlahRoom.setText("1");
                    } else {
                        int mod = esti%timexx;
                        if(mod!=0){
                            esti/=timexx;
                            esti*=timexx;
                            esti+=timexx;
                            Log.d("esti", String.valueOf(esti));
                        }
                        hasil =((esti / timexx) * hargaxx) * totroom;
                        add_RoomHasil.setText(String.valueOf("Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(String.valueOf(hasil)))));
                    }
                }
            }
        });

        add_RoomBtnSaveRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (add_RoomEstimasiWaktu.getText().toString().isEmpty()) {
                    add_RoomEstimasiWaktu.setError("Please input estimated cleaning in minutes!");
                    Toast.makeText(ListRoomActivity.this, "Please input estimated cleaning in minutes!", Toast.LENGTH_LONG).show();
                } else if (add_RoomJumlahRoom.getText().toString().isEmpty()) {
                    add_RoomJumlahRoom.setText("1");
                    Toast.makeText(ListRoomActivity.this, "Please input total room!", Toast.LENGTH_LONG).show();
                } else {
                    int esti = Integer.parseInt(add_RoomEstimasiWaktu.getText().toString().trim());
                    int totroom = Integer.parseInt(add_RoomJumlahRoom.getText().toString().trim());
                    int timexx = Integer.parseInt(xxtime);
                    int hargaxx = Integer.parseInt(xxharga);
                    int hasil = 0;
                    if (esti<timexx) {
                        add_RoomEstimasiWaktu.setError("Minimum should be "+timexx+" minutes!");
                        Toast.makeText(ListRoomActivity.this, "Minimum should be "+timexx+" minutes!", Toast.LENGTH_LONG).show();
                    } else if (totroom < 1) {
                        add_RoomJumlahRoom.setText("1");
                    } else {
                        int mod = esti%timexx;
                        if(mod!=0){
                            esti/=timexx;
                            esti*=timexx;
                            esti+=timexx;
                            Log.d("esti", String.valueOf(esti));
                        }
                        hasil =((esti / timexx) * hargaxx) * totroom;
                        add_RoomHasil.setText(String.valueOf("Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(String.valueOf(hasil)))));
                        //save sqlite
                        DbRoom db = new DbRoom(ListRoomActivity.this);
                        db.open();
                        db.insertRoom(xxid, xxnama, esti, totroom, hasil);
                        db.close();
                        RoomActivity.RoomActivity.finish();
                        finish();
                        Intent a = new Intent(ListRoomActivity.this,RoomActivity.class);
                        startActivity(a);
                    }
                }
            }
        });

        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
        //dialog.show();
    }

    private void setNUll() {
        add_RoomEstimasiWaktu.setText(null);
        add_RoomJumlahRoom.setText(null);
    }

    @Override
    protected void onResume() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        loadRoom();
        super.onResume();
    }

    private void loadRoom() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        swipe.setRefreshing(true);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Server.room_load + KategoriRoomActivity.xId_kategori_room, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            Log.e("loadRoom1", response.toString());
                            JSONArray jsonArrayContent = response.getJSONArray("content");
                            for(int i=0; i<jsonArrayContent.length();i++){
                                try {
                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String id = jsonObjectContent.getString("id");
                                    String id_area = jsonObjectContent.getString("id_area");
                                    String nama = jsonObjectContent.getString("nama");
                                    String des = jsonObjectContent.getString("des");
                                    String time = jsonObjectContent.getString("time");
                                    String harga = jsonObjectContent.getString("harga");
                                    String area = jsonObjectContent.getString("area");

                                    dataRoom item = new dataRoom();
                                    item.setId(id);
                                    item.setId_area(id_area);
                                    item.setNama(nama);
                                    item.setDes(des);
                                    item.setTime(time);
                                    item.setHarga(harga);
                                    item.setArea(area);
                                    itemList.add(item);
                                    Log.d("list_room",id+id_area+nama+des+time+harga+area);

                                    adapter.notifyDataSetChanged();
                                    swipe.setRefreshing(false);
                                }catch (JSONException e){
                                    e.printStackTrace();
                                    swipe.setRefreshing(false);
                                }
                            }
                        }
                        catch (JSONException e){
                            Log.e("error", e.getMessage());
                            swipe.setRefreshing(false);
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage(), getApplication());
                swipe.setRefreshing(false);
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    @Override
    public void onRefresh() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        loadRoom();
    }
}
