package mcs.mitratama.com.mcshomecleaning;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import mcs.mitratama.com.mcshomecleaning.util.Server;

public class ForgetPasswordActivity extends AppCompatActivity {

    EditText txtForgetEmail, txtForgetNewPassword;
    Button btnForgetResetPassword;

    private JSONObject jsonObject;
    private ProgressDialog dialog = null;
    Boolean success;
    String info;

    private JSONObject jsonObject2;
    private ProgressDialog dialog2 = null;
    Boolean success2;
    String info2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Check Email ...");
        dialog.setCancelable(false);

        dialog2 = new ProgressDialog(this);
        dialog2.setMessage("Reset now ...");
        dialog2.setCancelable(false);

        txtForgetEmail = (EditText)findViewById(R.id.txtForgetEmail);
        txtForgetNewPassword = (EditText)findViewById(R.id.txtForgetNewPassword);
        btnForgetResetPassword = (Button)findViewById(R.id.btnForgetResetPassword);

        btnForgetResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtForgetEmail.getText().toString().equals("")){
                    txtForgetEmail.setError("Email is null!");Toast.makeText(getApplication(),"Email is null!",Toast.LENGTH_SHORT).show();
                }else if(isValidEmaillId(txtForgetEmail.getText().toString().trim())==false){
                    txtForgetEmail.setError("Invalid email address!");Toast.makeText(getApplication(),"Invalid email address!",Toast.LENGTH_LONG).show();
                }else if(txtForgetNewPassword.getText().toString().equals("")){
                    txtForgetNewPassword.setError("New Password is null!");Toast.makeText(getApplication(),"New Password is null!",Toast.LENGTH_SHORT).show();
                }else if(txtForgetNewPassword.length() < 5){
                    txtForgetNewPassword.setError("New Password must be 5 character!");Toast.makeText(getApplication(),"New Password must be 5 character!",Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(getApplication(),"OKE!",Toast.LENGTH_SHORT).show();
                    cek_email(txtForgetEmail.getText().toString());
                }
            }
        });
    }

    private boolean isValidEmaillId(String email){
        return Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches();
    }

    private void cek_email(final String email) {
        jsonObject = new JSONObject();
        dialog.show();
        try {
            jsonObject.put("email", email.trim());
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.cek_email,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                dialog.dismiss();
                try {
                    success = jsonObject.getBoolean("success");
                    info = jsonObject.getString("info");
                    Log.e("okeSaveIt", success+", "+info);
                    if(success==true){
                        Toast.makeText(getApplication(), "Successful! [1]", Toast.LENGTH_SHORT).show();
                        reset_now(email,txtForgetNewPassword.getText().toString());
                    }else {
                        Toast.makeText(getApplication(), "Failed! [1]", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
                dialog.dismiss();
            }
        });jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    private void reset_now(final String email, final String new_password) {
        jsonObject2 = new JSONObject();
        dialog2.show();
        try {
            jsonObject2.put("email", email.trim());
            jsonObject2.put("password", new_password.trim());
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Server.forget_password,
                jsonObject2, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                dialog2.dismiss();
                try {
                    success2 = jsonObject.getBoolean("success");
                    info2 = jsonObject.getString("info");
                    Log.e("okeSaveIt", success2+", "+info2);
                    if(success2==true){
                        Toast.makeText(getApplication(), "Successful! [2]", Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        Toast.makeText(getApplication(), "Failed! [2]", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplication(), "Error to connect!", Toast.LENGTH_SHORT).show();
                Log.e("Internet Server Error", volleyError.toString());
                dialog2.dismiss();
            }
        });jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }


}
