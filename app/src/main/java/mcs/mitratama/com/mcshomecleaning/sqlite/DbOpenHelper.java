package mcs.mitratama.com.mcshomecleaning.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DbOpenHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "toriateccom.db";
	private static final String TABLE_AKUN_CREATE = "CREATE TABLE tb_room (id INTEGER PRIMARY KEY AUTOINCREMENT, id_room TEXT, nama TEXT, waktuestimasi INTEGER, totalroom INTEGER, totalbayar INTEGER)";

	public DbOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_AKUN_CREATE);

		//Contoh Insert
		/*ContentValues cv = new ContentValues();
		cv.put("idmupi", "TES");
		db.insert("tb_favorite", null, cv);*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
		db.execSQL("DROP TABLE IF EXISTS tb_room");
		onCreate(db);
	}

}
