package mcs.mitratama.com.mcshomecleaning.data;

/**
 * Created by koko on 7/13/16.
 */
public class dataHistory {
    private String id,id_invoice,id_user,id_pekerja,dated,timed,lat,lot,alamat,detail_alamat,total_pembayaran,total_estimasi_waktu,total_jumlah_room,flag,note,type_payment,review,review_detail,post_date, nama_pekerja, gender_cleaner;

    public dataHistory() {
    }

    public dataHistory(String id, String id_invoice, String id_user, String id_pekerja, String dated, String timed, String lat, String lot, String alamat, String detail_alamat, String total_pembayaran, String total_estimasi_waktu, String total_jumlah_room, String flag, String note, String type_payment, String review, String review_detail, String post_date, String nama_pekerja, String gender_cleaner) {
        this.id = id;
        this.id_invoice = id_invoice;
        this.id_user = id_user;
        this.id_pekerja = id_pekerja;
        this.dated = dated;
        this.timed = timed;
        this.lat = lat;
        this.lot = lot;
        this.alamat = alamat;
        this.detail_alamat = detail_alamat;
        this.total_pembayaran = total_pembayaran;
        this.total_estimasi_waktu = total_estimasi_waktu;
        this.total_jumlah_room = total_jumlah_room;
        this.flag = flag;
        this.note = note;
        this.type_payment = type_payment;
        this.review = review;
        this.review_detail = review_detail;
        this.post_date = post_date;
        this.nama_pekerja = nama_pekerja;
        this.gender_cleaner = gender_cleaner;
    }

    public String getGender_cleaner() {
        return gender_cleaner;
    }

    public void setGender_cleaner(String gender_cleaner) {
        this.gender_cleaner = gender_cleaner;
    }

    public String getNama_pekerja() {
        return nama_pekerja;
    }

    public void setNama_pekerja(String nama_pekerja) {
        this.nama_pekerja = nama_pekerja;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_invoice() {
        return id_invoice;
    }

    public void setId_invoice(String id_invoice) {
        this.id_invoice = id_invoice;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_pekerja() {
        return id_pekerja;
    }

    public void setId_pekerja(String id_pekerja) {
        this.id_pekerja = id_pekerja;
    }

    public String getDated() {
        return dated;
    }

    public void setDated(String dated) {
        this.dated = dated;
    }

    public String getTimed() {
        return timed;
    }

    public void setTimed(String timed) {
        this.timed = timed;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDetail_alamat() {
        return detail_alamat;
    }

    public void setDetail_alamat(String detail_alamat) {
        this.detail_alamat = detail_alamat;
    }

    public String getTotal_pembayaran() {
        return total_pembayaran;
    }

    public void setTotal_pembayaran(String total_pembayaran) {
        this.total_pembayaran = total_pembayaran;
    }

    public String getTotal_estimasi_waktu() {
        return total_estimasi_waktu;
    }

    public void setTotal_estimasi_waktu(String total_estimasi_waktu) {
        this.total_estimasi_waktu = total_estimasi_waktu;
    }

    public String getTotal_jumlah_room() {
        return total_jumlah_room;
    }

    public void setTotal_jumlah_room(String total_jumlah_room) {
        this.total_jumlah_room = total_jumlah_room;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getType_payment() {
        return type_payment;
    }

    public void setType_payment(String type_payment) {
        this.type_payment = type_payment;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getReview_detail() {
        return review_detail;
    }

    public void setReview_detail(String review_detail) {
        this.review_detail = review_detail;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }
}
