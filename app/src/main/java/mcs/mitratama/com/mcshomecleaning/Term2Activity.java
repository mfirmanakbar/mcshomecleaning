package mcs.mitratama.com.mcshomecleaning;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class Term2Activity extends AppCompatActivity {

    WebView myWebView;
    ProgressBar progressBar;
    String linknya="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);

        myWebView = (WebView) findViewById(R.id.webViewTerm);
        progressBar = (ProgressBar) findViewById(R.id.horizontal_progress_bar);
        myWebView.setWebViewClient(new myWebClient());
        myWebView.getSettings().setJavaScriptEnabled(true);
        if(InfoAwalActivity.linkVal=="faq"){
            setTitle("Frequently Asked Questions");
            linknya = "http://ptmitratama.com/mcs/terms/faq.html";
        }else if(InfoAwalActivity.linkVal=="terms"){
            setTitle("Term of Services");
            linknya = "http://ptmitratama.com/mcs/terms/terms.html";
        }
        myWebView.loadUrl(linknya);
    }

    public class myWebClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
