package mcs.mitratama.com.mcshomecleaning.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mcs.mitratama.com.mcshomecleaning.R;
import mcs.mitratama.com.mcshomecleaning.data.dataSqliteRoom;

/**
 * Created by koko on 7/10/16.
 */
public class AdapterSqliteRoom extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<dataSqliteRoom> items;

    public AdapterSqliteRoom(Activity activity, ArrayList<dataSqliteRoom> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_room,null);
        }

        TextView itemRoomId = (TextView) convertView.findViewById(R.id.itemRoomId);
        TextView itemRoomTotalRoom = (TextView) convertView.findViewById(R.id.itemRoomTotalRoom);
        TextView itemRoomName = (TextView) convertView.findViewById(R.id.itemRoomName);
        TextView itemRoomWaktuEstimasi = (TextView) convertView.findViewById(R.id.itemRoomWaktuEstimasi);
        TextView itemRoomTotalBayar = (TextView) convertView.findViewById(R.id.itemRoomTotalBayar);

        dataSqliteRoom data = items.get(position);
        itemRoomId.setText(data.getId_room());
        itemRoomName.setText(data.getNama());
        itemRoomWaktuEstimasi.setText(data.getWaktuestimasi()+" Minutes");
        itemRoomTotalRoom.setText(data.getTotalroom()+"");
        itemRoomTotalBayar.setText("Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(String.valueOf(data.getTotalbayar()))));

        return convertView;
    }
}
