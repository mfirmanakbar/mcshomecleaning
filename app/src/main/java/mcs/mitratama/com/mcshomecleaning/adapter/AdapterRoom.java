package mcs.mitratama.com.mcshomecleaning.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import mcs.mitratama.com.mcshomecleaning.R;
import mcs.mitratama.com.mcshomecleaning.data.dataRoom;

/**
 * Created by koko on 7/10/16.
 */
public class AdapterRoom extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<dataRoom> items;

    public AdapterRoom(Activity activity, List<dataRoom> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_list_room,null);
        }

        TextView itemListRoomName = (TextView) convertView.findViewById(R.id.itemListRoomName);
        TextView itemListRoomHargaJam = (TextView) convertView.findViewById(R.id.itemListRoomHargaJam);
        TextView itemListRoomArea = (TextView) convertView.findViewById(R.id.itemListRoomArea);

        dataRoom data = items.get(position);
        itemListRoomName.setText(data.getNama());
        itemListRoomHargaJam.setText("Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(data.getHarga()))+" /"+data.getTime()+" minutes");
        itemListRoomArea.setText(data.getArea());

        return convertView;
    }
}
